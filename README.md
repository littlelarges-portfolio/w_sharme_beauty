# W Sharme Beauty

## 📌 Features

- Auth
- Posts

also

- DDD
- ValueObjects
- Adaptive layout
- State Managment
- Localization
- Dependency Injection

## 🔎 Fields validations

        email:

            example@email.com ✅

            example@email ❌
            example.com ❌

        password:

            123456 ✅

            12345 ❌

        fullname:

             Full Name ✅

             full Name ❌

        username:

            username ✅

            123username ❌

        cityname:

            Cityname ✅

            cityname ❌

## ⚙️ Nuances

- Posts (you can create by taping "Опубликовать". It will create a new post, but, can't see a post in the UI. Then you can refresh profile page and you will see how count of posts changes)

## 🛠️ Core Technology Stack

| flutter_bloc | get_it | go_router |
|-------------|-------------|-------------|
| equatable | freezed | easy_localization |

## 📱 Screens

| Страница без ошибок       | Страница с ошибками         |
|---------------------------|-----------------------------|
| **Логин**                 | **Логин (ошибка валидации)**|
| ![](./readme_files/login_page.png) | ![](./readme_files/login_failed_validation_fields.png) |
| **Регистрация**           | **Регистрация (ошибка валидации)**|
| ![](./readme_files/register_page.png) | ![](./readme_files/register_failed_validation_fields.png) |
| **Данные пользователя**   | **Данные пользователя (ошибка валидации)**|
| ![](./readme_files/userdata_page.png) | ![](./readme_files/userdata_failed_validation_fields.png) |
| **Главная**               |     |
| ![](./readme_files/home_page.png) |  |
| **Профиль пользователя**               |    |
| ![](./readme_files/profile_page.png) |  |
| **Публикация поста**               |    |
| <img src="readme_files/posts_demo.gif"> |  |