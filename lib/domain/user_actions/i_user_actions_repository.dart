// ignore_for_file: one_member_abstracts

import 'package:kt_dart/kt.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';

abstract class IUserActionsRepository {
  /// return type is [UserActionsRemoteRequest]
  Future<UserActionsRemoteRequest<KtList<Post>>> fetchPosts({
    required EmailAddress email,
  });
}
