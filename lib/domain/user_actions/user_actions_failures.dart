import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_actions_failures.freezed.dart';

@freezed
abstract class UserActionsFailure with _$UserActionsFailure {
  const factory UserActionsFailure.serverError() = _ServerError;
}
