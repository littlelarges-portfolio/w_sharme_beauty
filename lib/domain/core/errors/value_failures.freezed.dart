// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'value_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ValueFailure<T> {
  Object get f => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AuthValueFailure<T> f) authValueFailure,
    required TResult Function(PostValueFailure<T> f) postValueFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AuthValueFailure<T> f)? authValueFailure,
    TResult? Function(PostValueFailure<T> f)? postValueFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AuthValueFailure<T> f)? authValueFailure,
    TResult Function(PostValueFailure<T> f)? postValueFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthValueFailure<T> value) authValueFailure,
    required TResult Function(_PostValueFailure<T> value) postValueFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult? Function(_PostValueFailure<T> value)? postValueFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult Function(_PostValueFailure<T> value)? postValueFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res, ValueFailure<T>>;
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res, $Val extends ValueFailure<T>>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AuthValueFailureImplCopyWith<T, $Res> {
  factory _$$AuthValueFailureImplCopyWith(_$AuthValueFailureImpl<T> value,
          $Res Function(_$AuthValueFailureImpl<T>) then) =
      __$$AuthValueFailureImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({AuthValueFailure<T> f});

  $AuthValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class __$$AuthValueFailureImplCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res, _$AuthValueFailureImpl<T>>
    implements _$$AuthValueFailureImplCopyWith<T, $Res> {
  __$$AuthValueFailureImplCopyWithImpl(_$AuthValueFailureImpl<T> _value,
      $Res Function(_$AuthValueFailureImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_$AuthValueFailureImpl<T>(
      null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as AuthValueFailure<T>,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $AuthValueFailureCopyWith<T, $Res> get f {
    return $AuthValueFailureCopyWith<T, $Res>(_value.f, (value) {
      return _then(_value.copyWith(f: value));
    });
  }
}

/// @nodoc

class _$AuthValueFailureImpl<T> implements _AuthValueFailure<T> {
  const _$AuthValueFailureImpl(this.f);

  @override
  final AuthValueFailure<T> f;

  @override
  String toString() {
    return 'ValueFailure<$T>.authValueFailure(f: $f)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthValueFailureImpl<T> &&
            (identical(other.f, f) || other.f == f));
  }

  @override
  int get hashCode => Object.hash(runtimeType, f);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthValueFailureImplCopyWith<T, _$AuthValueFailureImpl<T>> get copyWith =>
      __$$AuthValueFailureImplCopyWithImpl<T, _$AuthValueFailureImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AuthValueFailure<T> f) authValueFailure,
    required TResult Function(PostValueFailure<T> f) postValueFailure,
  }) {
    return authValueFailure(f);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AuthValueFailure<T> f)? authValueFailure,
    TResult? Function(PostValueFailure<T> f)? postValueFailure,
  }) {
    return authValueFailure?.call(f);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AuthValueFailure<T> f)? authValueFailure,
    TResult Function(PostValueFailure<T> f)? postValueFailure,
    required TResult orElse(),
  }) {
    if (authValueFailure != null) {
      return authValueFailure(f);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthValueFailure<T> value) authValueFailure,
    required TResult Function(_PostValueFailure<T> value) postValueFailure,
  }) {
    return authValueFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult? Function(_PostValueFailure<T> value)? postValueFailure,
  }) {
    return authValueFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult Function(_PostValueFailure<T> value)? postValueFailure,
    required TResult orElse(),
  }) {
    if (authValueFailure != null) {
      return authValueFailure(this);
    }
    return orElse();
  }
}

abstract class _AuthValueFailure<T> implements ValueFailure<T> {
  const factory _AuthValueFailure(final AuthValueFailure<T> f) =
      _$AuthValueFailureImpl<T>;

  @override
  AuthValueFailure<T> get f;
  @JsonKey(ignore: true)
  _$$AuthValueFailureImplCopyWith<T, _$AuthValueFailureImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PostValueFailureImplCopyWith<T, $Res> {
  factory _$$PostValueFailureImplCopyWith(_$PostValueFailureImpl<T> value,
          $Res Function(_$PostValueFailureImpl<T>) then) =
      __$$PostValueFailureImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({PostValueFailure<T> f});

  $PostValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class __$$PostValueFailureImplCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res, _$PostValueFailureImpl<T>>
    implements _$$PostValueFailureImplCopyWith<T, $Res> {
  __$$PostValueFailureImplCopyWithImpl(_$PostValueFailureImpl<T> _value,
      $Res Function(_$PostValueFailureImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_$PostValueFailureImpl<T>(
      null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as PostValueFailure<T>,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $PostValueFailureCopyWith<T, $Res> get f {
    return $PostValueFailureCopyWith<T, $Res>(_value.f, (value) {
      return _then(_value.copyWith(f: value));
    });
  }
}

/// @nodoc

class _$PostValueFailureImpl<T> implements _PostValueFailure<T> {
  const _$PostValueFailureImpl(this.f);

  @override
  final PostValueFailure<T> f;

  @override
  String toString() {
    return 'ValueFailure<$T>.postValueFailure(f: $f)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PostValueFailureImpl<T> &&
            (identical(other.f, f) || other.f == f));
  }

  @override
  int get hashCode => Object.hash(runtimeType, f);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PostValueFailureImplCopyWith<T, _$PostValueFailureImpl<T>> get copyWith =>
      __$$PostValueFailureImplCopyWithImpl<T, _$PostValueFailureImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AuthValueFailure<T> f) authValueFailure,
    required TResult Function(PostValueFailure<T> f) postValueFailure,
  }) {
    return postValueFailure(f);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AuthValueFailure<T> f)? authValueFailure,
    TResult? Function(PostValueFailure<T> f)? postValueFailure,
  }) {
    return postValueFailure?.call(f);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AuthValueFailure<T> f)? authValueFailure,
    TResult Function(PostValueFailure<T> f)? postValueFailure,
    required TResult orElse(),
  }) {
    if (postValueFailure != null) {
      return postValueFailure(f);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_AuthValueFailure<T> value) authValueFailure,
    required TResult Function(_PostValueFailure<T> value) postValueFailure,
  }) {
    return postValueFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult? Function(_PostValueFailure<T> value)? postValueFailure,
  }) {
    return postValueFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_AuthValueFailure<T> value)? authValueFailure,
    TResult Function(_PostValueFailure<T> value)? postValueFailure,
    required TResult orElse(),
  }) {
    if (postValueFailure != null) {
      return postValueFailure(this);
    }
    return orElse();
  }
}

abstract class _PostValueFailure<T> implements ValueFailure<T> {
  const factory _PostValueFailure(final PostValueFailure<T> f) =
      _$PostValueFailureImpl<T>;

  @override
  PostValueFailure<T> get f;
  @JsonKey(ignore: true)
  _$$PostValueFailureImplCopyWith<T, _$PostValueFailureImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AuthValueFailure<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthValueFailureCopyWith<T, $Res> {
  factory $AuthValueFailureCopyWith(
          AuthValueFailure<T> value, $Res Function(AuthValueFailure<T>) then) =
      _$AuthValueFailureCopyWithImpl<T, $Res, AuthValueFailure<T>>;
}

/// @nodoc
class _$AuthValueFailureCopyWithImpl<T, $Res, $Val extends AuthValueFailure<T>>
    implements $AuthValueFailureCopyWith<T, $Res> {
  _$AuthValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InvalidEmailImplCopyWith<T, $Res> {
  factory _$$InvalidEmailImplCopyWith(_$InvalidEmailImpl<T> value,
          $Res Function(_$InvalidEmailImpl<T>) then) =
      __$$InvalidEmailImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$InvalidEmailImplCopyWithImpl<T, $Res>
    extends _$AuthValueFailureCopyWithImpl<T, $Res, _$InvalidEmailImpl<T>>
    implements _$$InvalidEmailImplCopyWith<T, $Res> {
  __$$InvalidEmailImplCopyWithImpl(
      _$InvalidEmailImpl<T> _value, $Res Function(_$InvalidEmailImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$InvalidEmailImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidEmailImpl<T> implements _InvalidEmail<T> {
  const _$InvalidEmailImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'AuthValueFailure<$T>.invalidEmail(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidEmailImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidEmailImplCopyWith<T, _$InvalidEmailImpl<T>> get copyWith =>
      __$$InvalidEmailImplCopyWithImpl<T, _$InvalidEmailImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) {
    return invalidEmail(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) {
    return invalidEmail?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) {
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) {
    return invalidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }
}

abstract class _InvalidEmail<T> implements AuthValueFailure<T> {
  const factory _InvalidEmail({required final T failedValue}) =
      _$InvalidEmailImpl<T>;

  T get failedValue;
  @JsonKey(ignore: true)
  _$$InvalidEmailImplCopyWith<T, _$InvalidEmailImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShortPasswordImplCopyWith<T, $Res> {
  factory _$$ShortPasswordImplCopyWith(_$ShortPasswordImpl<T> value,
          $Res Function(_$ShortPasswordImpl<T>) then) =
      __$$ShortPasswordImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({T shortPassword});
}

/// @nodoc
class __$$ShortPasswordImplCopyWithImpl<T, $Res>
    extends _$AuthValueFailureCopyWithImpl<T, $Res, _$ShortPasswordImpl<T>>
    implements _$$ShortPasswordImplCopyWith<T, $Res> {
  __$$ShortPasswordImplCopyWithImpl(_$ShortPasswordImpl<T> _value,
      $Res Function(_$ShortPasswordImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shortPassword = freezed,
  }) {
    return _then(_$ShortPasswordImpl<T>(
      shortPassword: freezed == shortPassword
          ? _value.shortPassword
          : shortPassword // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$ShortPasswordImpl<T> implements _ShortPassword<T> {
  const _$ShortPasswordImpl({required this.shortPassword});

  @override
  final T shortPassword;

  @override
  String toString() {
    return 'AuthValueFailure<$T>.shortPassword(shortPassword: $shortPassword)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShortPasswordImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.shortPassword, shortPassword));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(shortPassword));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShortPasswordImplCopyWith<T, _$ShortPasswordImpl<T>> get copyWith =>
      __$$ShortPasswordImplCopyWithImpl<T, _$ShortPasswordImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) {
    return shortPassword(this.shortPassword);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) {
    return shortPassword?.call(this.shortPassword);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) {
    if (shortPassword != null) {
      return shortPassword(this.shortPassword);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) {
    return shortPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) {
    return shortPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) {
    if (shortPassword != null) {
      return shortPassword(this);
    }
    return orElse();
  }
}

abstract class _ShortPassword<T> implements AuthValueFailure<T> {
  const factory _ShortPassword({required final T shortPassword}) =
      _$ShortPasswordImpl<T>;

  T get shortPassword;
  @JsonKey(ignore: true)
  _$$ShortPasswordImplCopyWith<T, _$ShortPasswordImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvalidFullnameImplCopyWith<T, $Res> {
  factory _$$InvalidFullnameImplCopyWith(_$InvalidFullnameImpl<T> value,
          $Res Function(_$InvalidFullnameImpl<T>) then) =
      __$$InvalidFullnameImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$InvalidFullnameImplCopyWithImpl<T, $Res>
    extends _$AuthValueFailureCopyWithImpl<T, $Res, _$InvalidFullnameImpl<T>>
    implements _$$InvalidFullnameImplCopyWith<T, $Res> {
  __$$InvalidFullnameImplCopyWithImpl(_$InvalidFullnameImpl<T> _value,
      $Res Function(_$InvalidFullnameImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$InvalidFullnameImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidFullnameImpl<T> implements _InvalidFullname<T> {
  const _$InvalidFullnameImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'AuthValueFailure<$T>.invalidFullname(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidFullnameImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidFullnameImplCopyWith<T, _$InvalidFullnameImpl<T>> get copyWith =>
      __$$InvalidFullnameImplCopyWithImpl<T, _$InvalidFullnameImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) {
    return invalidFullname(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) {
    return invalidFullname?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidFullname != null) {
      return invalidFullname(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) {
    return invalidFullname(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) {
    return invalidFullname?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidFullname != null) {
      return invalidFullname(this);
    }
    return orElse();
  }
}

abstract class _InvalidFullname<T> implements AuthValueFailure<T> {
  const factory _InvalidFullname({required final T failedValue}) =
      _$InvalidFullnameImpl<T>;

  T get failedValue;
  @JsonKey(ignore: true)
  _$$InvalidFullnameImplCopyWith<T, _$InvalidFullnameImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvalidUsernameImplCopyWith<T, $Res> {
  factory _$$InvalidUsernameImplCopyWith(_$InvalidUsernameImpl<T> value,
          $Res Function(_$InvalidUsernameImpl<T>) then) =
      __$$InvalidUsernameImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$InvalidUsernameImplCopyWithImpl<T, $Res>
    extends _$AuthValueFailureCopyWithImpl<T, $Res, _$InvalidUsernameImpl<T>>
    implements _$$InvalidUsernameImplCopyWith<T, $Res> {
  __$$InvalidUsernameImplCopyWithImpl(_$InvalidUsernameImpl<T> _value,
      $Res Function(_$InvalidUsernameImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$InvalidUsernameImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidUsernameImpl<T> implements _InvalidUsername<T> {
  const _$InvalidUsernameImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'AuthValueFailure<$T>.invalidUsername(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidUsernameImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidUsernameImplCopyWith<T, _$InvalidUsernameImpl<T>> get copyWith =>
      __$$InvalidUsernameImplCopyWithImpl<T, _$InvalidUsernameImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) {
    return invalidUsername(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) {
    return invalidUsername?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidUsername != null) {
      return invalidUsername(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) {
    return invalidUsername(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) {
    return invalidUsername?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidUsername != null) {
      return invalidUsername(this);
    }
    return orElse();
  }
}

abstract class _InvalidUsername<T> implements AuthValueFailure<T> {
  const factory _InvalidUsername({required final T failedValue}) =
      _$InvalidUsernameImpl<T>;

  T get failedValue;
  @JsonKey(ignore: true)
  _$$InvalidUsernameImplCopyWith<T, _$InvalidUsernameImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InvalidCitynameImplCopyWith<T, $Res> {
  factory _$$InvalidCitynameImplCopyWith(_$InvalidCitynameImpl<T> value,
          $Res Function(_$InvalidCitynameImpl<T>) then) =
      __$$InvalidCitynameImplCopyWithImpl<T, $Res>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$InvalidCitynameImplCopyWithImpl<T, $Res>
    extends _$AuthValueFailureCopyWithImpl<T, $Res, _$InvalidCitynameImpl<T>>
    implements _$$InvalidCitynameImplCopyWith<T, $Res> {
  __$$InvalidCitynameImplCopyWithImpl(_$InvalidCitynameImpl<T> _value,
      $Res Function(_$InvalidCitynameImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$InvalidCitynameImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InvalidCitynameImpl<T> implements _InvalidCityname<T> {
  const _$InvalidCitynameImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'AuthValueFailure<$T>.invalidCityname(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidCitynameImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidCitynameImplCopyWith<T, _$InvalidCitynameImpl<T>> get copyWith =>
      __$$InvalidCitynameImplCopyWithImpl<T, _$InvalidCitynameImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) invalidEmail,
    required TResult Function(T shortPassword) shortPassword,
    required TResult Function(T failedValue) invalidFullname,
    required TResult Function(T failedValue) invalidUsername,
    required TResult Function(T failedValue) invalidCityname,
  }) {
    return invalidCityname(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? invalidEmail,
    TResult? Function(T shortPassword)? shortPassword,
    TResult? Function(T failedValue)? invalidFullname,
    TResult? Function(T failedValue)? invalidUsername,
    TResult? Function(T failedValue)? invalidCityname,
  }) {
    return invalidCityname?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? invalidEmail,
    TResult Function(T shortPassword)? shortPassword,
    TResult Function(T failedValue)? invalidFullname,
    TResult Function(T failedValue)? invalidUsername,
    TResult Function(T failedValue)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidCityname != null) {
      return invalidCityname(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_InvalidEmail<T> value) invalidEmail,
    required TResult Function(_ShortPassword<T> value) shortPassword,
    required TResult Function(_InvalidFullname<T> value) invalidFullname,
    required TResult Function(_InvalidUsername<T> value) invalidUsername,
    required TResult Function(_InvalidCityname<T> value) invalidCityname,
  }) {
    return invalidCityname(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_InvalidEmail<T> value)? invalidEmail,
    TResult? Function(_ShortPassword<T> value)? shortPassword,
    TResult? Function(_InvalidFullname<T> value)? invalidFullname,
    TResult? Function(_InvalidUsername<T> value)? invalidUsername,
    TResult? Function(_InvalidCityname<T> value)? invalidCityname,
  }) {
    return invalidCityname?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_InvalidEmail<T> value)? invalidEmail,
    TResult Function(_ShortPassword<T> value)? shortPassword,
    TResult Function(_InvalidFullname<T> value)? invalidFullname,
    TResult Function(_InvalidUsername<T> value)? invalidUsername,
    TResult Function(_InvalidCityname<T> value)? invalidCityname,
    required TResult orElse(),
  }) {
    if (invalidCityname != null) {
      return invalidCityname(this);
    }
    return orElse();
  }
}

abstract class _InvalidCityname<T> implements AuthValueFailure<T> {
  const factory _InvalidCityname({required final T failedValue}) =
      _$InvalidCitynameImpl<T>;

  T get failedValue;
  @JsonKey(ignore: true)
  _$$InvalidCitynameImplCopyWith<T, _$InvalidCitynameImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PostValueFailure<T> {
  T get failedValue => throw _privateConstructorUsedError;
  int get max => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue, int max) contentIsTooLong,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue, int max)? contentIsTooLong,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue, int max)? contentIsTooLong,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ContentIsTooLong<T> value) contentIsTooLong,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ContentIsTooLong<T> value)? contentIsTooLong,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ContentIsTooLong<T> value)? contentIsTooLong,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PostValueFailureCopyWith<T, PostValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostValueFailureCopyWith<T, $Res> {
  factory $PostValueFailureCopyWith(
          PostValueFailure<T> value, $Res Function(PostValueFailure<T>) then) =
      _$PostValueFailureCopyWithImpl<T, $Res, PostValueFailure<T>>;
  @useResult
  $Res call({T failedValue, int max});
}

/// @nodoc
class _$PostValueFailureCopyWithImpl<T, $Res, $Val extends PostValueFailure<T>>
    implements $PostValueFailureCopyWith<T, $Res> {
  _$PostValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
    Object? max = null,
  }) {
    return _then(_value.copyWith(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
      max: null == max
          ? _value.max
          : max // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ContentIsTooLongImplCopyWith<T, $Res>
    implements $PostValueFailureCopyWith<T, $Res> {
  factory _$$ContentIsTooLongImplCopyWith(_$ContentIsTooLongImpl<T> value,
          $Res Function(_$ContentIsTooLongImpl<T>) then) =
      __$$ContentIsTooLongImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue, int max});
}

/// @nodoc
class __$$ContentIsTooLongImplCopyWithImpl<T, $Res>
    extends _$PostValueFailureCopyWithImpl<T, $Res, _$ContentIsTooLongImpl<T>>
    implements _$$ContentIsTooLongImplCopyWith<T, $Res> {
  __$$ContentIsTooLongImplCopyWithImpl(_$ContentIsTooLongImpl<T> _value,
      $Res Function(_$ContentIsTooLongImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
    Object? max = null,
  }) {
    return _then(_$ContentIsTooLongImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
      max: null == max
          ? _value.max
          : max // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ContentIsTooLongImpl<T> implements _ContentIsTooLong<T> {
  const _$ContentIsTooLongImpl({required this.failedValue, required this.max});

  @override
  final T failedValue;
  @override
  final int max;

  @override
  String toString() {
    return 'PostValueFailure<$T>.contentIsTooLong(failedValue: $failedValue, max: $max)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ContentIsTooLongImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue) &&
            (identical(other.max, max) || other.max == max));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue), max);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ContentIsTooLongImplCopyWith<T, _$ContentIsTooLongImpl<T>> get copyWith =>
      __$$ContentIsTooLongImplCopyWithImpl<T, _$ContentIsTooLongImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue, int max) contentIsTooLong,
  }) {
    return contentIsTooLong(failedValue, max);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue, int max)? contentIsTooLong,
  }) {
    return contentIsTooLong?.call(failedValue, max);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue, int max)? contentIsTooLong,
    required TResult orElse(),
  }) {
    if (contentIsTooLong != null) {
      return contentIsTooLong(failedValue, max);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ContentIsTooLong<T> value) contentIsTooLong,
  }) {
    return contentIsTooLong(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ContentIsTooLong<T> value)? contentIsTooLong,
  }) {
    return contentIsTooLong?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ContentIsTooLong<T> value)? contentIsTooLong,
    required TResult orElse(),
  }) {
    if (contentIsTooLong != null) {
      return contentIsTooLong(this);
    }
    return orElse();
  }
}

abstract class _ContentIsTooLong<T> implements PostValueFailure<T> {
  const factory _ContentIsTooLong(
      {required final T failedValue,
      required final int max}) = _$ContentIsTooLongImpl<T>;

  @override
  T get failedValue;
  @override
  int get max;
  @override
  @JsonKey(ignore: true)
  _$$ContentIsTooLongImplCopyWith<T, _$ContentIsTooLongImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
