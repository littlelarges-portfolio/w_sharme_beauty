import 'package:freezed_annotation/freezed_annotation.dart';

part 'value_failures.freezed.dart';

@freezed
sealed class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.authValueFailure(AuthValueFailure<T> f) =
      _AuthValueFailure<T>;
  const factory ValueFailure.postValueFailure(PostValueFailure<T> f) =
      _PostValueFailure<T>;
}

@freezed
sealed class AuthValueFailure<T> with _$AuthValueFailure<T> {
  const factory AuthValueFailure.invalidEmail({required T failedValue}) =
      _InvalidEmail<T>;
  const factory AuthValueFailure.shortPassword({required T shortPassword}) =
      _ShortPassword<T>;
  const factory AuthValueFailure.invalidFullname({required T failedValue}) =
      _InvalidFullname<T>;
  const factory AuthValueFailure.invalidUsername({required T failedValue}) =
      _InvalidUsername<T>;
  const factory AuthValueFailure.invalidCityname({required T failedValue}) =
      _InvalidCityname<T>;
}

@freezed
sealed class PostValueFailure<T> with _$PostValueFailure<T> {
  const factory PostValueFailure.contentIsTooLong({
    required T failedValue,
    required int max,
  }) = _ContentIsTooLong<T>;
}
