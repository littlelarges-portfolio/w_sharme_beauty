import 'package:dartz/dartz.dart';
import 'package:w_sharme_beauty/domain/auth/auth_failures.dart';
import 'package:w_sharme_beauty/domain/post/post_failures.dart';
import 'package:w_sharme_beauty/domain/user_actions/user_actions_failures.dart';

typedef AuthResult = Either<AuthFailure, Unit>;
typedef PostResult = Either<PostFailure, Unit>;

typedef AuthResultOption = Option<AuthResult>;

typedef PostRemoteRequest = Either<PostFailure, Unit>;
typedef UserActionsRemoteRequest<T> = Either<UserActionsFailure, T>;
