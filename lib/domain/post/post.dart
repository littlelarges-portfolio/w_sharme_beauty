import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:w_sharme_beauty/domain/core/errors/value_failures.dart';
import 'package:w_sharme_beauty/domain/core/value_objects.dart';
import 'package:w_sharme_beauty/domain/post/post_value_objects.dart';

part 'post.freezed.dart';

@freezed
abstract class Post with _$Post {
  const factory Post({
    required UniqueId id,
    required UniqueId userId,
    required Content content,
    required int likesCount,
  }) = _Post;

  const Post._();

  factory Post.empty() => Post(
        id: UniqueId(),
        userId: UniqueId.fromUniqueString(''),
        content: Content('empty content'),
        likesCount: 23,
      );

  Option<ValueFailure<dynamic>> get failureOption {
    return content.failureOrUnit.fold(some, (_) => none());
  }
}
