import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';

abstract class IPostRepository {
  /// return type is [PostResult]
  Future<PostResult> publish(Post post);
  /// return type is [PostResult]
  Future<PostResult> delete(Post post);
}
