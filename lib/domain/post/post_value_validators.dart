import 'package:dartz/dartz.dart';
import 'package:w_sharme_beauty/domain/core/errors/value_failures.dart';

Either<ValueFailure<String>, String> validateContent(
  String input,
  int maxLength,
) {
  if (input.length <= maxLength) {
    return right(input);
  } else {
    return left(
      ValueFailure.postValueFailure(
        PostValueFailure.contentIsTooLong(failedValue: input, max: maxLength),
      ),
    );
  }
}
