import 'package:w_sharme_beauty/domain/core/value_objects.dart';
import 'package:w_sharme_beauty/domain/post/post_value_validators.dart';

class Content extends ValueObject<String> {
  factory Content(String input) => Content._(validateContent(input, maxLength));

  const Content._(super.value);

  static const maxLength = 1000;
}
