// ignore_for_file: lines_longer_than_80_chars

import 'package:dartz/dartz.dart';
import 'package:w_sharme_beauty/domain/core/errors/value_failures.dart';

Either<ValueFailure<String>, String> validateEmailAddress(String input) {
  const emailRegex =
      r"""^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+""";

  if (RegExp(emailRegex).hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.invalidEmail(failedValue: input),
      ),
    );
  }
}

Either<ValueFailure<String>, String> validatePassword(String input) {
  if (input.length >= 6) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.shortPassword(shortPassword: input),
      ),
    );
  }
}

Either<ValueFailure<String>, String> validateFullname(String input) {
  final fullnameRegExp = RegExp(r'^[А-ЯЁA-Z][а-яёa-z]+ [А-ЯЁA-Z][а-яёa-z]+$');

  /**
  [fullnameRegExp]: 
   
  1. Full name begins with a capital letter (Russian English alphabet);
  2. After the first capital letter, only lowercase letters will follow (as in Full Name);
  3. Each part of the name is separated by a space.
  */

  if (fullnameRegExp.hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.invalidFullname(failedValue: input),
      ),
    );
  }
}

Either<ValueFailure<String>, String> validateUsername(String input) {
  final usernameRegExp = RegExp(r'^[a-zA-Z][a-zA-Z0-9._]{1,}$');

  /**
  [usernameRegExp]:

  1. The username begins with a letter (upper or lower case);
  2. The username can use letters (upper and lower case), numbers, periods and underscores;
  3. The username must contain at least one character after the first character;
  4. The username must not begin with a period, number, or underscore.
 */

  if (usernameRegExp.hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.invalidUsername(failedValue: input),
      ),
    );
  }
}

Either<ValueFailure<String>, String> validateCityname(String input) {
  final citynameRegExp = RegExp(r'^[А-ЯЁA-Z][а-яёA-Za-z ]+$');

  /**
  [citynameRegExp]
  
  1. The name of the city begins with a capital letter (Russian and English alphabet);
  2. The first capital letter can only be followed by letters (upper and lower case), spaces;
  3. The city name must contain at least one character after the first capital letter.
  */

  if (citynameRegExp.hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.invalidCityname(failedValue: input),
      ),
    );
  }
}

Either<ValueFailure<String>, String> validateProfilePicture(String input) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(
      ValueFailure.authValueFailure(
        AuthValueFailure.invalidCityname(failedValue: input),
      ),
    );
  }
}
