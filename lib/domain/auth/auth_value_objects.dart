import 'package:w_sharme_beauty/domain/auth/auth_value_validators.dart';
import 'package:w_sharme_beauty/domain/core/value_objects.dart';

class EmailAddress extends ValueObject<String> {
  factory EmailAddress(String input) =>
      EmailAddress._(validateEmailAddress(input));

  const EmailAddress._(super.value);
}

class Password extends ValueObject<String> {
  factory Password(String input) => Password._(validatePassword(input));

  const Password._(super.value);
}

class Fullname extends ValueObject<String> {
  factory Fullname(String input) => Fullname._(validateFullname(input));

  const Fullname._(super.value);
}

class Username extends ValueObject<String> {
  factory Username(String input) => Username._(validateUsername(input));

  const Username._(super.value);
}

class Cityname extends ValueObject<String> {
  factory Cityname(String input) => Cityname._(validateCityname(input));

  const Cityname._(super.value);
}

class ProfilePicture extends ValueObject<String> {
  factory ProfilePicture(String input) =>
      ProfilePicture._(validateProfilePicture(input));

  const ProfilePicture._(super.value);
}
