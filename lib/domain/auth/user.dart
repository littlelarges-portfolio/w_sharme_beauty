import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';

part 'user.freezed.dart';

@freezed
abstract class User with _$User {
  const factory User({
    required EmailAddress email,
  }) = _User;
}
