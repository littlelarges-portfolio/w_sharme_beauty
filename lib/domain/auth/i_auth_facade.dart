import 'package:dartz/dartz.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/auth/user.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';

abstract class IAuthFacade {
  Future<AuthResult> login({
    required EmailAddress email,
    required Password password,
  });
  Future<AuthResult> register({
    required EmailAddress email,
    required Password password,
  });
  Future<AuthResult> saveUserData({
    required EmailAddress email,
    required Fullname fullname,
    required Username username,
    required Cityname cityname,
  });
  Future<Option<User>> getSignedInUser();
  Future<void> signOut();
}
