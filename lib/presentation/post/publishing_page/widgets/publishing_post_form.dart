import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/post/post_form_bloc.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/post/publishing_page/widgets/content_field_widget.dart';

class PublishingPostForm extends StatelessWidget {
  const PublishingPostForm({required this.toolbarHeight, super.key});

  final double toolbarHeight;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 18.r),
      child: Form(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: SizedBox(
            width: 1.sw,
            height:
                ScreenSizeHelper.getScreenHeightWithBottomBar - toolbarHeight,
            child: Column(
              children: [
                ContentField(publishPressed: _publishPressed),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                        onPressed: () => _publishPressed(context),
                        child: Text(
                          LocaleKeys.publishing_post_page_publishButton.tr(),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _publishPressed(BuildContext context) {
    context.read<PostFormBloc>().add(const PostFormEvent.published());
  }
}
