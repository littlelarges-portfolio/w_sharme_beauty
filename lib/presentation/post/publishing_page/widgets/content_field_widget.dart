import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/post/post_form_bloc.dart';
import 'package:w_sharme_beauty/domain/post/post_value_objects.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';

class ContentField extends StatelessWidget {
  const ContentField({
    required this.publishPressed,
    super.key,
  });

  final void Function(BuildContext context) publishPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          LocaleKeys.publishing_post_page_content.tr(),
          style: TextStyle(
            fontSize: 14.r,
            fontWeight: FontWeight.w500,
            color: Colours.grey,
          ),
        ),
        SizedBox(height: 10.r),
        TextFormField(
          decoration: InputDecoration(
            hintText: LocaleKeys.publishing_post_page_contentHint.tr(),
            // to hide symbols count
            counterText: '',
          ),
          onChanged: (value) => context
              .read<PostFormBloc>()
              .add(PostFormEvent.contentChanged(value)),
          validator: (_) => context
              .read<PostFormBloc>()
              .state
              .post
              .content
              .value
              .fold(
                (valueFailure) => valueFailure.maybeWhen(
                  postValueFailure: (postValueFailure) =>
                      postValueFailure.maybeWhen(
                    contentIsTooLong: (f, max) => 'Exceeding length! Max: $max',
                    orElse: () => null,
                  ),
                  orElse: () => null,
                ),
                (r) => null,
              ),
          onFieldSubmitted: (_) => publishPressed(context),
          maxLength: Content.maxLength,
          minLines: 2,
          maxLines: 8,
        ),
      ],
    );
  }
}
