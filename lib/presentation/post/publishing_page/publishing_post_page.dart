import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/post/post_form_bloc.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/post/publishing_page/widgets/publishing_post_form.dart';

class PublishingPostPage extends StatelessWidget {
  const PublishingPostPage({super.key});
  double get _appBarHeight => 80.r;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<PostFormBloc>(),
      child: Scaffold(
        backgroundColor: Colours.white,
        appBar: AppBar(
          title: Text(
            LocaleKeys.publishing_post_page_title.tr(),
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.r),
          ),
          toolbarHeight: _appBarHeight,
        ),
        body: PublishingPostForm(
          toolbarHeight: _appBarHeight,
        ),
      ),
    );
  }
}
