import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';

class FeedPage extends StatelessWidget {
  const FeedPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SizedBox(
            width: 1.sw,
            height: ScreenSizeHelper.getCleanScreenHeight,
            child: Center(
              child: Text(
                'Feed',
                style: TextStyle(fontSize: 50.r),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
