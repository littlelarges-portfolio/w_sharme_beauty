part of 'router.dart';

@TypedGoRoute<LoginRoute>(
  path: RoutesPath.auth_login,
)
class LoginRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) => const LoginPage();
}

@TypedGoRoute<RegisterRoute>(
  path: RoutesPath.auth_register,
)
class RegisterRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const RegisterPage();
}

@TypedGoRoute<SaveUserdataRoute>(
  path: RoutesPath.auth_saveUserdata,
)
class SaveUserdataRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const SaveUserdataPage();
}
