part of 'router.dart';

@TypedStatefulShellRoute<ShellRoute>(
  branches: <TypedStatefulShellBranch<StatefulShellBranchData>>[
    TypedStatefulShellBranch<FeedBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<FeedRoute>(path: RoutesPath.shell_feed),
      ],
    ),
    TypedStatefulShellBranch<ProfileBranchRoute>(
      routes: <TypedRoute<RouteData>>[
        TypedGoRoute<ProfileRoute>(path: RoutesPath.shell_profile),
      ],
    ),
  ],
)
class ShellRoute extends StatefulShellRouteData {
  const ShellRoute();

  @override
  Widget builder(
    BuildContext context,
    GoRouterState state,
    StatefulNavigationShell navigationShell,
  ) {
    return navigationShell;
  }

  static const String $restorationScopeId = 'restorationScopeId';

  static Widget $navigatorContainerBuilder(
    BuildContext context,
    StatefulNavigationShell navigationShell,
    List<Widget> children,
  ) {
    return ShellPage(
      navigationShell: navigationShell,
      children: children,
    );
  }
}

class FeedBranchRoute extends StatefulShellBranchData {
  const FeedBranchRoute();
}

class ProfileBranchRoute extends StatefulShellBranchData {
  const ProfileBranchRoute();
}

class FeedRoute extends GoRouteData {
  const FeedRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const FeedPage();
}

class ProfileRoute extends GoRouteData {
  const ProfileRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const ProfilePage();
}
