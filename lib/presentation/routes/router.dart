import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:w_sharme_beauty/presentation/auth/login/login_page.dart';
import 'package:w_sharme_beauty/presentation/auth/register/register_page.dart';
import 'package:w_sharme_beauty/presentation/auth/register/save_userdata_page.dart';
import 'package:w_sharme_beauty/presentation/feed/feed_page.dart';
import 'package:w_sharme_beauty/presentation/post/publishing_page/publishing_post_page.dart';
import 'package:w_sharme_beauty/presentation/profile/profile_page.dart';
import 'package:w_sharme_beauty/presentation/routes/core/routes_path.dart';
import 'package:w_sharme_beauty/presentation/shell/shell_page.dart';

part 'auth_routes.dart';
part 'shell_routes.dart';
part 'profile_routes.dart';

part 'router.g.dart';

final router = GoRouter(
  routes: $appRoutes,
  initialLocation: RoutesPath.auth_login,
);
