part of 'router.dart';

@TypedGoRoute<PublishingPostRoute>(
  path: RoutesPath.profile_publishingPost,
)
class PublishingPostRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const PublishingPostPage();
}
