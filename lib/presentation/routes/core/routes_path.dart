// ignore_for_file: constant_identifier_names

class RoutesPath {
  static const auth_login = '/login';
  static const auth_register = '/register';
  static const auth_saveUserdata = '/saveUserdata';

  static const shell_feed = '/feed';
  static const shell_profile = '/profile';

  static const profile_publishingPost = '/publishingPost';
}

enum Some { one, two }
