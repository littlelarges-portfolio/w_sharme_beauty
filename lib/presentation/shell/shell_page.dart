import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:w_sharme_beauty/presentation/core/assets/assets_path.dart';

class ShellPage extends StatelessWidget {
  const ShellPage({
    required this.navigationShell,
    required this.children,
    super.key,
  });

  final StatefulNavigationShell navigationShell;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset(AssetPaths.home_icon_png),
            activeIcon: Image.asset(AssetPaths.home_active_icon_png),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Image.asset(AssetPaths.profile_icon_png),
            activeIcon: Image.asset(AssetPaths.profile_active_icon_png),
            label: 'Profile',
          ),
        ],
        currentIndex: navigationShell.currentIndex,
        onTap: (int index) {
          navigationShell.goBranch(
            index,
            initialLocation: index == navigationShell.currentIndex,
          );
        },
      ),
      body: IndexedStack(
        index: navigationShell.currentIndex,
        children: children,
      ),
    );
  }
}
