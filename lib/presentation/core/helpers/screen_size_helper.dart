import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenSizeHelper {
  static double get getCleanScreenHeight =>
      ScreenUtil().screenHeight -
      ScreenUtil().statusBarHeight -
      ScreenUtil().bottomBarHeight -
      kToolbarHeight;

  static double get getScreenHeightWithBottomBar =>
      ScreenUtil().screenHeight -
      ScreenUtil().statusBarHeight -
      ScreenUtil().bottomBarHeight;
}
