// ignore_for_file: constant_identifier_names

class AssetPaths {
  static const register_agreement_checked_svg =
      'assets/svg/auth/register/agreement_checked.svg';
  static const register_agreement_unchecked_svg =
      'assets/svg/auth/register/agreement_unchecked.svg';

  static const notification_icon_png = 'assets/images/icons/profile/notifications.png';
  static const settings_icon_png = 'assets/images/icons/profile/settings.png';
  static const post_icon_png = 'assets/images/icons/profile/post.png';

  static const home_icon_png = 'assets/images/icons/shell/home.png';
  static const home_active_icon_png = 'assets/images/icons/shell/home_active.png';
  static const profile_icon_png = 'assets/images/icons/shell/profile.png';
  static const profile_active_icon_png = 'assets/images/icons/shell/profile_active.png';
}
