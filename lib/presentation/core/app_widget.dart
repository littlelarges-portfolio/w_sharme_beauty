import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/auth/bloc/auth_bloc.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/fonts/font_families.dart';
import 'package:w_sharme_beauty/presentation/routes/router.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(430, 932),
      splitScreenMode: true,
      minTextAdapt: true,
      builder: (context, child) => BlocProvider(
        create: (context) =>
            getIt<AuthBloc>()..add(const AuthEvent.authCheckRequested()),
        child: MaterialApp.router(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          debugShowCheckedModeBanner: false,
          routerConfig: router,
          theme: ThemeData(
            appBarTheme: const AppBarTheme(backgroundColor: Colours.white),
            scaffoldBackgroundColor: Colours.background,
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(double.infinity, 47.r),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                backgroundColor: Colours.primary,
                foregroundColor: Colours.white,
                shadowColor: Colors.transparent,
                surfaceTintColor: Colors.transparent,
                disabledBackgroundColor: Colours.grey,
                disabledForegroundColor: Colours.greyBackgroundForBlocks,
              ),
            ),
            colorScheme: ColorScheme.fromSeed(seedColor: Colours.primary),
            useMaterial3: true,
            inputDecorationTheme: InputDecorationTheme(
              contentPadding: EdgeInsets.all(20.r),
              fillColor: Colours.greyBackgroundForBlocks,
              hintStyle: TextStyle(
                color: Colours.grey,
                fontWeight: FontWeight.w500,
                fontSize: 16.r,
              ),
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
            ),
            iconTheme: IconTheme.of(context).copyWith(size: 24.r),
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(
                  displayLarge: TextStyle(fontSize: 57.r),
                  displayMedium: TextStyle(fontSize: 45.r),
                  displaySmall: TextStyle(fontSize: 36.r),
                  headlineLarge: TextStyle(fontSize: 32.r),
                  headlineMedium: TextStyle(fontSize: 28.r),
                  headlineSmall: TextStyle(fontSize: 24.r),
                  titleLarge: TextStyle(fontSize: 22.r),
                  titleMedium: TextStyle(fontSize: 16.r),
                  titleSmall: TextStyle(fontSize: 14.r),
                  labelLarge: TextStyle(fontSize: 14.r),
                  labelMedium: TextStyle(fontSize: 12.r),
                  labelSmall: TextStyle(fontSize: 11.r),
                  bodyLarge: TextStyle(fontSize: 16.r),
                  bodyMedium: TextStyle(fontSize: 14.r),
                  bodySmall: TextStyle(fontSize: 12.r),
                )
                .apply(
                  fontFamily: FontFamilies.gtEestiProDisplay,
                  bodyColor: Colours.text,
                  displayColor: Colours.text,
                ),
          ),
        ),
      ),
    );
  }
}
