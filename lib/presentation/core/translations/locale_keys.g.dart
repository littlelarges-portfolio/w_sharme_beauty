// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const login_page_title = 'login_page.title';
  static const login_page_description = 'login_page.description';
  static const login_page_emailHint = 'login_page.emailHint';
  static const login_page_passwordHint = 'login_page.passwordHint';
  static const login_page_forgotPassword = 'login_page.forgotPassword';
  static const login_page = 'login_page';
  static const register_page_title = 'register_page.title';
  static const register_page_description = 'register_page.description';
  static const register_page_emailHint = 'register_page.emailHint';
  static const register_page_passwordHint = 'register_page.passwordHint';
  static const register_page_agreementRegular = 'register_page.agreementRegular';
  static const register_page_agreementStyled = 'register_page.agreementStyled';
  static const register_page = 'register_page';
  static const save_userdata_page_title = 'save_userdata_page.title';
  static const save_userdata_page_description = 'save_userdata_page.description';
  static const save_userdata_page_fullnameHint = 'save_userdata_page.fullnameHint';
  static const save_userdata_page_usernameHint = 'save_userdata_page.usernameHint';
  static const save_userdata_page_citynameHint = 'save_userdata_page.citynameHint';
  static const save_userdata_page = 'save_userdata_page';
  static const personal_profile_page_title = 'personal_profile_page.title';
  static const personal_profile_page_posts = 'personal_profile_page.posts';
  static const personal_profile_page_followers = 'personal_profile_page.followers';
  static const personal_profile_page_follows = 'personal_profile_page.follows';
  static const personal_profile_page_editProfile = 'personal_profile_page.editProfile';
  static const personal_profile_page_publishButton = 'personal_profile_page.publishButton';
  static const personal_profile_page = 'personal_profile_page';
  static const publishing_post_page_title = 'publishing_post_page.title';
  static const publishing_post_page_content = 'publishing_post_page.content';
  static const publishing_post_page_contentHint = 'publishing_post_page.contentHint';
  static const publishing_post_page_publishButton = 'publishing_post_page.publishButton';
  static const publishing_post_page = 'publishing_post_page';
  static const buttons_login = 'buttons.login';
  static const buttons_register = 'buttons.register';
  static const buttons_continue = 'buttons.continue';
  static const buttons_save = 'buttons.save';
  static const buttons = 'buttons';

}
