// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes, avoid_renaming_method_parameters

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>?> load(String path, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ru = {
  "login_page": {
    "title": "Авторизация",
    "description": "Что-бы пользоваться сервисом на любом устройстве.",
    "emailHint": "Эл.адрес",
    "passwordHint": "Введите пароль",
    "forgotPassword": "Забыли пароль?"
  },
  "register_page": {
    "title": "Регистрация",
    "description": "Введите электронный адрес и придумайте пароль.",
    "emailHint": "Эл.адрес",
    "passwordHint": "Придумайте пароль",
    "agreementRegular": "Я даю согласие на ",
    "agreementStyled": "рассылку уведомлений."
  },
  "save_userdata_page": {
    "title": "Данные профиля",
    "description": "Заполните личные данные, чтобы иметь доступ к своим заказам и результатам в приложении",
    "fullnameHint": "Ваше Ф.И.О.",
    "usernameHint": "Придумайте имя пользователя",
    "citynameHint": "Ваш город"
  },
  "personal_profile_page": {
    "title": "Профиль",
    "posts": "Публикации",
    "followers": "Подписчиков",
    "follows": "Подписок",
    "edit_profile": "Редактировать профиль",
    "post": "Опубликовать"
  },
  "buttons": {
    "login": "Войти",
    "register": "Регистрация",
    "continue": "Продолжить",
    "save": "Сохранить"
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ru": ru};
}
