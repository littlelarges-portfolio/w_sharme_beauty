import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Colours {
  static final text = HexColor('0A0A0A');
  static final primary = HexColor('BA75FE');
  static final background = HexColor('F9FAFF');
  static const white = Colors.white;
  static final grey = HexColor('A2A8B0');
  static final greyBackgroundForBlocks = HexColor('F3F3F3');
  static final red = HexColor('FE7575');
}
