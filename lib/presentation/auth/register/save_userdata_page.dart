import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:w_sharme_beauty/application/auth/register_form/register_form_bloc.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/auth/register/widgets/save_userdata_form.dart';

class SaveUserdataPage extends StatelessWidget {
  const SaveUserdataPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<RegisterFormBloc>(),
      child: const Scaffold(
        body: SaveUserdataForm(),
      ),
    );
  }
}
