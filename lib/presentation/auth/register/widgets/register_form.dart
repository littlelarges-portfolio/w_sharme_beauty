import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:w_sharme_beauty/application/auth/register_form/register_form_bloc.dart';
import 'package:w_sharme_beauty/presentation/core/assets/assets_path.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/routes/router.dart';

class RegisterForm extends HookWidget {
  const RegisterForm({super.key});

  @override
  Widget build(BuildContext context) {
    final passwordVisible = useState(false);
    final agreementMarked = useState(false);

    return BlocConsumer<RegisterFormBloc, RegisterFormState>(
      listener: (context, state) {
        state.registerResultOption.fold(
          () {},
          (authResultEither) => authResultEither.fold(
            (f) => FlushbarHelper.createError(
              message: f.map(
                serverError: (_) => 'Server Error!',
                emailAlreadyInUse: (_) => 'Email already in use',
                invalidEmailAndPasswordCombination: (_) =>
                    'Invalid email and password combination',
                userNotFound: (_) => 'User not found!',
              ),
            ).show(context),
            (_) {
              SaveUserdataRoute().push<SaveUserdataRoute>(context);
            },
          ),
        );
      },
      builder: (context, state) {
        return SafeArea(
          child: Padding(
            padding: EdgeInsets.only(
              right: 18.r,
              left: 18.r,
            ),
            child: Form(
              autovalidateMode: state.showErrorMessages,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  width: 1.sw,
                  height: ScreenSizeHelper.getScreenHeightWithBottomBar,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 230.r),
                          Text(
                            LocaleKeys.register_page_title.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 38.r,
                              height: 1.3.r,
                            ),
                          ),
                          SizedBox(height: 20.r),
                          Text(
                            LocaleKeys.register_page_description.tr(),
                            style: TextStyle(
                              fontSize: 16.r,
                            ),
                          ),
                          SizedBox(height: 30.r),
                          TextFormField(
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: LocaleKeys.register_page_emailHint.tr(),
                            ),
                            onChanged: (value) {
                              context
                                  .read<RegisterFormBloc>()
                                  .add(RegisterFormEvent.emailChanged(value));
                            },
                            validator: (value) => context
                                .read<RegisterFormBloc>()
                                .state
                                .emailAddress
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      invalidEmail: (_) => 'Invalid Email',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 10.r),
                          TextFormField(
                            obscureText: !passwordVisible.value,
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: (_) {
                              if (agreementMarked.value) {
                                _continuePressed(context);
                              }
                            },
                            decoration: InputDecoration(
                              hintText:
                                  LocaleKeys.register_page_passwordHint.tr(),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  passwordVisible.value =
                                      !passwordVisible.value;
                                },
                                // TODO(littlelarge): change icons to actual
                                icon: Icon(
                                  passwordVisible.value
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                ),
                              ),
                            ),
                            onChanged: (value) {
                              context.read<RegisterFormBloc>().add(
                                    RegisterFormEvent.passwordChanged(value),
                                  );
                            },
                            validator: (_) => context
                                .read<RegisterFormBloc>()
                                .state
                                .password
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      shortPassword: (_) => 'Short Password',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 16.r),
                          Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  agreementMarked.value =
                                      !agreementMarked.value;
                                },
                                overlayColor: const MaterialStatePropertyAll(
                                  Colors.transparent,
                                ),
                                child: SizedBox(
                                  height: 26.r,
                                  width: 26.r,
                                  child: AnimatedSwitcher(
                                    duration: const Duration(milliseconds: 75),
                                    transitionBuilder: (child, animation) =>
                                        ScaleTransition(
                                      scale: animation,
                                      child: child,
                                    ),
                                    child: agreementMarked.value
                                        ? SvgPicture.asset(
                                            key: const Key('checked'),
                                            AssetPaths
                                                .register_agreement_checked_svg,
                                          )
                                        : SvgPicture.asset(
                                            key: const Key('unchecked'),
                                            AssetPaths
                                                // ignore: lines_longer_than_80_chars
                                                .register_agreement_unchecked_svg,
                                          ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10.r,
                              ),
                              Flexible(
                                child: Row(
                                  children: [
                                    Text(
                                      LocaleKeys.register_page_agreementRegular
                                          .tr(),
                                      style: TextStyle(
                                        color: Colours.text,
                                        fontSize: 14.r,
                                      ),
                                    ),
                                    TextButton(
                                      style: const ButtonStyle(
                                        minimumSize:
                                            MaterialStatePropertyAll(Size.zero),
                                        padding: MaterialStatePropertyAll(
                                          EdgeInsets.zero,
                                        ),
                                        overlayColor: MaterialStatePropertyAll(
                                          Colors.transparent,
                                        ),
                                      ),
                                      onPressed: () {
                                        // ignore: lines_longer_than_80_chars
                                        // TODO(littlelarge): implement redirecting to link
                                      },
                                      child: Text(
                                        LocaleKeys.register_page_agreementStyled
                                            .tr(),
                                        style: TextStyle(
                                          color: Colours.primary,
                                          fontSize: 14.r,
                                          decoration: TextDecoration.underline,
                                          decorationColor: Colours.primary,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              onPressed: agreementMarked.value
                                  ? () => _continuePressed(context)
                                  : null,
                              child: Text(
                                LocaleKeys.buttons_continue.tr(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _continuePressed(BuildContext context) =>
      context.read<RegisterFormBloc>().add(
            const RegisterFormEvent.continuePressed(),
          );
}
