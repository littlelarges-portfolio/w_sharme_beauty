import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:w_sharme_beauty/application/auth/register_form/register_form_bloc.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';

class SaveUserdataForm extends StatelessWidget {
  const SaveUserdataForm({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegisterFormBloc, RegisterFormState>(
      listener: (context, state) {
        state.registerResultOption.fold(
          () {},
          (authResultEither) => authResultEither.fold(
            (f) => FlushbarHelper.createError(
              message: f.map(
                serverError: (_) => 'Server Error!',
                emailAlreadyInUse: (_) => 'Email already in use',
                invalidEmailAndPasswordCombination: (_) =>
                    'Invalid email and password combination',
                userNotFound: (_) => 'User not found!',
              ),
            ).show(context),
            (_) {
              // TODO(littlelarge): implement navigation to main

              context.go('/feed');
            },
          ),
        );
      },
      builder: (context, state) {
        return SafeArea(
          child: Padding(
            padding: EdgeInsets.only(
              right: 18.r,
              left: 18.r,
            ),
            child: Form(
              autovalidateMode: state.showErrorMessages,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  width: 1.sw,
                  height: ScreenSizeHelper.getScreenHeightWithBottomBar,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 230.r),
                          Text(
                            LocaleKeys.save_userdata_page_title.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 38.r,
                              height: 1.3.r,
                            ),
                          ),
                          SizedBox(height: 20.r),
                          Text(
                            LocaleKeys.save_userdata_page_description.tr(),
                            style: TextStyle(
                              fontSize: 16.r,
                            ),
                          ),
                          SizedBox(height: 30.r),
                          TextFormField(
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: LocaleKeys
                                  .save_userdata_page_fullnameHint
                                  .tr(),
                            ),
                            onChanged: (value) {
                              context.read<RegisterFormBloc>().add(
                                    RegisterFormEvent.fullnameChanged(value),
                                  );
                            },
                            validator: (value) => context
                                .read<RegisterFormBloc>()
                                .state
                                .fullname
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      invalidFullname: (_) =>
                                          'Invalid Fullname',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 10.r),
                          TextFormField(
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: LocaleKeys
                                  .save_userdata_page_usernameHint
                                  .tr(),
                            ),
                            onChanged: (value) {
                              context.read<RegisterFormBloc>().add(
                                    RegisterFormEvent.usernameChanged(value),
                                  );
                            },
                            validator: (_) => context
                                .read<RegisterFormBloc>()
                                .state
                                .username
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      invalidUsername: (_) =>
                                          'Invalid Username',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 10.r),
                          TextFormField(
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: (_) =>
                                _saveUserDataPressed(context),
                            decoration: InputDecoration(
                              hintText: LocaleKeys
                                  .save_userdata_page_citynameHint
                                  .tr(),
                            ),
                            onChanged: (value) {
                              context.read<RegisterFormBloc>().add(
                                    RegisterFormEvent.citynameChanged(value),
                                  );
                            },
                            validator: (_) => context
                                .read<RegisterFormBloc>()
                                .state
                                .cityname
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      invalidCityname: (_) =>
                                          'Invalid Cityname',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              onPressed: () => _saveUserDataPressed(context),
                              child: Text(
                                LocaleKeys.buttons_save.tr(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _saveUserDataPressed(BuildContext context) =>
      context.read<RegisterFormBloc>().add(
            const RegisterFormEvent.saveUserDataPressed(),
          );
}
