import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:w_sharme_beauty/application/auth/login_form/login_form_bloc.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/auth/login/widgets/login_form.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<LoginFormBloc>(),
      child: const Scaffold(
        body: LoginForm(),
      ),
    );
  }
}
