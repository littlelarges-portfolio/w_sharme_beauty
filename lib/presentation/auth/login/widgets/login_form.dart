import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/auth/bloc/auth_bloc.dart';
import 'package:w_sharme_beauty/application/auth/login_form/login_form_bloc.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/routes/router.dart';

class LoginForm extends HookWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final passwordVisible = useState(false);

    return BlocConsumer<LoginFormBloc, LoginFormState>(
      listener: (context, state) {
        state.loginResultOption.fold(
          () {},
          (authResultEither) => authResultEither.fold(
            (f) => FlushbarHelper.createError(
              message: f.map(
                serverError: (_) => 'Server Error!',
                emailAlreadyInUse: (_) => 'Email already in use',
                invalidEmailAndPasswordCombination: (_) =>
                    'Invalid email and password combination',
                userNotFound: (_) => 'User not found!',
              ),
            ).show(context),
            (r) {
              // TODO(littlelarge): implement navigation to main

              const FeedRoute().go(context);

              context
                  .read<AuthBloc>()
                  .add(const AuthEvent.authCheckRequested());
            },
          ),
        );
      },
      builder: (context, state) {
        return SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 18.r),
            child: Form(
              autovalidateMode: state.showErrorMessages,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  width: 1.sw,
                  height: ScreenSizeHelper.getScreenHeightWithBottomBar,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 230.r),
                          Text(
                            LocaleKeys.login_page_title.tr(),
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 38.r,
                              height: 1.3.r,
                            ),
                          ),
                          SizedBox(height: 20.r),
                          Text(
                            LocaleKeys.login_page_description.tr(),
                            style: TextStyle(
                              fontSize: 16.r,
                            ),
                          ),
                          SizedBox(height: 30.r),
                          TextFormField(
                            textInputAction: TextInputAction.next,
                            decoration: InputDecoration(
                              hintText: LocaleKeys.login_page_emailHint.tr(),
                            ),
                            onChanged: (value) {
                              context
                                  .read<LoginFormBloc>()
                                  .add(LoginFormEvent.emailChanged(value));
                            },
                            validator: (value) => context
                                .read<LoginFormBloc>()
                                .state
                                .emailAddress
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      invalidEmail: (_) => 'Invalid Email',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 10.r),
                          TextFormField(
                            obscureText: !passwordVisible.value,
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: (_) => _loginPressed(context),
                            decoration: InputDecoration(
                              hintText:
                                  LocaleKeys.login_page_passwordHint.tr(),
                              suffixIcon: IconButton(
                                onPressed: () {
                                  passwordVisible.value =
                                      !passwordVisible.value;
                                },
                                // TODO(littlelarge): change icons to actual
                                icon: Icon(
                                  passwordVisible.value
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                ),
                              ),
                            ),
                            onChanged: (value) {
                              context
                                  .read<LoginFormBloc>()
                                  .add(LoginFormEvent.passwordChanged(value));
                            },
                            validator: (_) => context
                                .read<LoginFormBloc>()
                                .state
                                .password
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                    authValueFailure: (authValueFailure) =>
                                        authValueFailure.f.maybeMap(
                                      shortPassword: (_) => 'Short Password',
                                      orElse: () => null,
                                    ),
                                    orElse: () => null,
                                  ),
                                  (r) => null,
                                ),
                          ),
                          SizedBox(height: 10.r),
                          Container(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              onPressed: () {
                                // TODO(littlelarge): implement forgot password
                              },
                              child: Text(
                                LocaleKeys.login_page_forgotPassword.tr(),
                                style: TextStyle(
                                  color: Colours.primary,
                                  fontSize: 14.r,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                              onPressed: () => _loginPressed(context),
                              child: Text(
                                LocaleKeys.buttons_login.tr(),
                              ),
                            ),
                            SizedBox(height: 6.r),
                            ElevatedButton(
                              onPressed: () {
                                RegisterRoute().go(context);
                              },
                              style: Theme.of(context)
                                  .elevatedButtonTheme
                                  .style
                                  ?.copyWith(
                                    backgroundColor:
                                        const MaterialStatePropertyAll(
                                      Colours.white,
                                    ),
                                    foregroundColor:
                                        MaterialStatePropertyAll(
                                      Colours.primary,
                                    ),
                                  ),
                              child: Text(
                                LocaleKeys.buttons_register.tr(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void _loginPressed(BuildContext context) =>
      context.read<LoginFormBloc>().add(const LoginFormEvent.loginPressed());
}
