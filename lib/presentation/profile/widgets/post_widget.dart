import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/post/post_form_bloc.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/core/assets/assets_path.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/routes/router.dart';

class Post extends StatelessWidget {
  const Post({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<PostFormBloc>(),
      child: BlocBuilder<PostFormBloc, PostFormState>(
        buildWhen: (previous, current) =>
            previous.isPublishing != current.isPublishing,
        builder: (context, state) {
          return Container(
            padding: EdgeInsets.all(18.r),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              color: Colours.white,
            ),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colours.primary.withOpacity(.1),
                foregroundColor: Colours.primary,
              ),
              onPressed: () {
                PublishingPostRoute().push<PublishingPostRoute>(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 20.r,
                    width: 20.r,
                    child: Image.asset(AssetPaths.post_icon_png),
                  ),
                  SizedBox(width: 12.r),
                  Text(
                    LocaleKeys.personal_profile_page_publishButton.tr(),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
