import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kt_dart/kt.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';

import 'package:w_sharme_beauty/presentation/profile/widgets/profile_info_widget.dart';

class Profile extends StatelessWidget {
  const Profile({
    required this.posts,
    super.key,
  });

  final KtList<Post> posts;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 18.r),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
        color: Colours.white,
      ),
      child: Column(
        children: [
          SizedBox(height: 23.r),
          ProfileInfo(
            postsCount: posts.size,
          ),
          SizedBox(height: 16.r),
        ],
      ),
    );
  }
}
