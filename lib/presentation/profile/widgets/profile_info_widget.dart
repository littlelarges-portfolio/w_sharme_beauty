import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';

class ProfileInfo extends StatelessWidget {
  const ProfileInfo({required this.postsCount, super.key});

  final int postsCount;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ClipOval(
          child: Image.network(
            'https://s3-alpha-sig.figma.com/img/66f8/583a/f3d2f8433a04549a27cfd1d220f37dd9?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=ZHTgonLfN~KkjxlMMbX3NKdJkiIKIZ4tBnyuasJGsmft7x8mOrml2rFydXQ99gN4bw~TR7jWeohErlcJw2IsXiA2lWEx8DoEbwQqyyKV7c5EPA62NS95yqujd7o~fZVfxafB4rf5-uiZ76FS0TeyPG2yfvaczUTk1Q-rZghZHyBD5eof2Yf2Oh1Bg0RSS6h3Li7yyzCVC3BUz3haGkHL5U-MoS1yE-h2JH0yeGwZLehiyA82N9ZQ3h1899XceXVz-BpbeFIsD9fF2g4ZJGPnyqkiumdc96LPcKjpYsTJcb0yUF9KCzM~eINcHcKoF83fEYs9DXlQakR-p7lQMW413Q__',
            height: 90.r,
            width: 90.r,
            fit: BoxFit.cover,
            errorBuilder: (context, error, stackTrace) => Container(),
          ),
        ),
        SizedBox(width: 20.r),
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ProfileInfoItem(
                title: LocaleKeys.personal_profile_page_posts.tr(),
                content: postsCount.toString(),
              ),
              ProfileInfoItem(
                title:
                    LocaleKeys.personal_profile_page_followers.tr(),
                content: '2422',
              ),
              ProfileInfoItem(
                title: LocaleKeys.personal_profile_page_follows.tr(),
                content: '51',
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class ProfileInfoItem extends StatelessWidget {
  const ProfileInfoItem({
    required this.title,
    required this.content,
    super.key,
  });

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colours.white,
      child: InkWell(
        onTap: () {
          // TODO(littlelarge): implement profile info item tap
        },
        child: Column(
          children: [
            Text(
              content,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 22.r,
              ),
            ),
            SizedBox(height: 5.r),
            Text(
              title,
              style: TextStyle(
                fontSize: 14.r,
                color: Colours.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
