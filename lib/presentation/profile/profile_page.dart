import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:w_sharme_beauty/application/auth/bloc/auth_bloc.dart';
import 'package:w_sharme_beauty/application/post/post_fetcher/posts_fetcher_bloc.dart';
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart';
import 'package:w_sharme_beauty/domain/core/errors/errors.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/core/assets/assets_path.dart';
import 'package:w_sharme_beauty/presentation/core/colours/colours.dart';
import 'package:w_sharme_beauty/presentation/core/helpers/screen_size_helper.dart';
import 'package:w_sharme_beauty/presentation/core/translations/locale_keys.g.dart';
import 'package:w_sharme_beauty/presentation/profile/widgets/post_widget.dart';
import 'package:w_sharme_beauty/presentation/profile/widgets/profile_widget.dart';
import 'package:w_sharme_beauty/presentation/routes/router.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  double get _appBarHeight => 80.r;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: _appBarHeight,
        title: Padding(
          padding: EdgeInsets.only(left: 18.r),
          child: Text(
            LocaleKeys.personal_profile_page_title.tr(),
            style: TextStyle(fontSize: 22.r, fontWeight: FontWeight.w500),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 18.r),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    // TODO(littlelarge): implement notifications button
                  },
                  customBorder: const CircleBorder(),
                  child: Image.asset(AssetPaths.notification_icon_png),
                ),
                SizedBox(width: 16.r),
                InkWell(
                  onTap: () {
                    // TODO(littlelarge): implement settings button
                  },
                  customBorder: const CircleBorder(),
                  child: Image.asset(AssetPaths.settings_icon_png),
                ),
              ],
            ),
          ),
        ],
        centerTitle: false,
      ),
      body: FutureBuilder(
        future: getIt<IAuthFacade>().getSignedInUser(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return BlocListener<AuthBloc, AuthState>(
              listener: (context, state) {
                state.maybeMap(
                  unauthenticated: (_) => LoginRoute().go(context),
                  orElse: () {},
                );
              },
              child: BlocProvider(
                create: (context) => getIt<PostsFetcherBloc>()
                  ..add(
                    PostsFetcherEvent.fetched(
                      emailAddress: snapshot.data!
                          .getOrElse(() => throw NotAuthenticatedError())
                          .email,
                    ),
                  ),
                child: BlocBuilder<PostsFetcherBloc, PostsFetcherState>(
                  builder: (context, state) {
                    return state.map(
                      initial: (_) => Container(),
                      fetchingInProgress: (_) =>
                          const CircularProgressIndicator(),
                      fetchingSuccess: (state) => RefreshIndicator(
                        onRefresh: () async {
                          context.read<PostsFetcherBloc>().add(
                                PostsFetcherEvent.fetched(
                                  emailAddress: snapshot.data!
                                      .getOrElse(
                                        () => throw UnimplementedError(),
                                      )
                                      .email,
                                ),
                              );
                        },
                        child: SingleChildScrollView(
                          physics: const AlwaysScrollableScrollPhysics(),
                          child: SizedBox(
                            height: ScreenSizeHelper.getCleanScreenHeight -
                                _appBarHeight,
                            child: Column(
                              children: [
                                Profile(posts: state.posts),
                                SizedBox(height: 15.r),
                                const Post(),
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 18.r),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Colours.red,
                                    ),
                                    onPressed: () {
                                      context
                                          .read<AuthBloc>()
                                          .add(const AuthEvent.signedOut());
                                    },
                                    child: const Text('Sign out'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      fetchingFailed: (f) => Container(),
                    );
                  },
                ),
              ),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
