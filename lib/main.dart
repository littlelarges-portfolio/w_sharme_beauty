import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:w_sharme_beauty/injection.dart';
import 'package:w_sharme_beauty/presentation/core/app_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  configureInjection(Environment.dev);

  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('ru')],
      path:
          'assets/translations',
      fallbackLocale: const Locale('ru'),
      child: const AppWidget(),
    ),
  );
}
