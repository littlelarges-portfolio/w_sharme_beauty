import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/auth/user.dart';

extension StringX on String {
  User toDomain() {
    return User(email: EmailAddress(this));
  }
}
