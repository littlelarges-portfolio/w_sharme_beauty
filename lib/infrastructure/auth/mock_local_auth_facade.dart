// ignore_for_file: constant_identifier_names

import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:w_sharme_beauty/domain/auth/auth_failures.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart';
import 'package:w_sharme_beauty/domain/auth/user.dart' as u;
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/infrastructure/auth/local_mock_mapper.dart';
import 'package:w_sharme_beauty/infrastructure/core/mock_local_helpers.dart';

const USER_NOT_EXISTS = 'user_not_exists';

@LazySingleton(as: IAuthFacade)
class MockLocalAuthFacade implements IAuthFacade {
  MockLocalAuthFacade(this._sharedPreferences);

  final SharedPreferences _sharedPreferences;

  @override
  Future<AuthResult> login({
    required EmailAddress email,
    required Password password,
  }) async {
    try {
      final emailStr = email.getOrCrash();
      final passwordStr = password.getOrCrash();

      final userId = _sharedPreferences.getUser(emailStr);
      final userExists = userId != null;
      final userPassword = _sharedPreferences.getUserPassword(emailStr);

      if (userExists) {
        if (userPassword == passwordStr) {
          await _sharedPreferences.login(emailStr, passwordStr);

          return right(unit);
        } else {
          return left(const AuthFailure.invalidEmailAndPasswordCombination());
        }
      } else {
        throw PlatformException(code: USER_NOT_EXISTS);
      }
    } on PlatformException catch (e) {
      if (e.code == USER_NOT_EXISTS) {
        return left(const AuthFailure.userNotFound());
      } else {
        return left(const AuthFailure.serverError());
      }
    }
  }

  @override
  Future<AuthResult> register({
    required EmailAddress email,
    required Password password,
  }) async {
    try {
      final emailStr = email.getOrCrash();
      final passwordStr = password.getOrCrash();

      await _sharedPreferences.registerUser(emailStr, passwordStr);

      return right(unit);
    } on PlatformException catch (_) {
      return left(const AuthFailure.serverError());
    }
  }

  @override
  Future<AuthResult> saveUserData({
    required EmailAddress email,
    required Fullname fullname,
    required Username username,
    required Cityname cityname,
  }) async {
    try {
      await Future.wait([
        _sharedPreferences.setUserData(
          email.getOrCrash(),
          UserDataPaths.fullname,
          fullname.getOrCrash(),
        ),
        _sharedPreferences.setUserData(
          email.getOrCrash(),
          UserDataPaths.username,
          username.getOrCrash(),
        ),
        _sharedPreferences.setUserData(
          email.getOrCrash(),
          UserDataPaths.cityname,
          cityname.getOrCrash(),
        ),
      ]);

      return right(unit);
    } on PlatformException catch (_) {
      return left(const AuthFailure.serverError());
    }
  }

  @override
  Future<Option<u.User>> getSignedInUser() async {
    final user = await _sharedPreferences.getSignedInUser();

    return optionOf(user?.toDomain());
  }

  @override
  Future<void> signOut() => _sharedPreferences.signOut();
}
