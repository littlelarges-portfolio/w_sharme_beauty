import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/domain/user_actions/i_user_actions_repository.dart';
import 'package:w_sharme_beauty/domain/user_actions/user_actions_failures.dart';
import 'package:w_sharme_beauty/infrastructure/core/mock_local_helpers.dart';

@LazySingleton(as: IUserActionsRepository)
class UserActionsRepository implements IUserActionsRepository {
  UserActionsRepository(this._sharedPreferences);

  final SharedPreferences _sharedPreferences;

  @override
  Future<UserActionsRemoteRequest<KtList<Post>>> fetchPosts({
    required EmailAddress email,
  }) async {
    try {
      final posts = await _sharedPreferences.getPosts(email.getOrCrash());

      return right<UserActionsFailure, KtList<Post>>(
        posts.map((postDto) => postDto.toDomain()).toImmutableList(),
      );
    } on PlatformException catch (_) {
      return left<UserActionsFailure, KtList<Post>>(
        const UserActionsFailure.serverError(),
      );
    }
  }
}
