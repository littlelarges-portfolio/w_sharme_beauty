import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:w_sharme_beauty/infrastructure/post/post_dto.dart';

const signedInUserKey = 'signed_in_user';

enum UserDataPaths {
  password,
  fullname,
  username,
  cityname,
  posts,
}

extension UserDataPathsExtension on UserDataPaths {
  String get value {
    switch (this) {
      case UserDataPaths.password:
        return 'password';
      case UserDataPaths.fullname:
        return 'fullname';
      case UserDataPaths.username:
        return 'username';
      case UserDataPaths.cityname:
        return 'cityname';
      case UserDataPaths.posts:
        return 'posts';
    }
  }
}

extension SharedPreferencesX on SharedPreferences {
  String? getUser(String emailStr) => getString(emailStr);

  String getUserPassword(String emailStr) =>
      getString('$emailStr/${UserDataPaths.password.value}') ?? '';

  Future<void> login(String emailStr, String passwordStr) async {
    await setString(signedInUserKey, emailStr);
  }

  Future<void> registerUser(String emailStr, String passwordStr) async {
    await Future.wait([
      setString(emailStr, emailStr),
      setString('$emailStr/${UserDataPaths.password.value}', passwordStr),
      setString(signedInUserKey, emailStr),
    ]);
  }

  Future<void> setUserData(
    String emailStr,
    UserDataPaths path,
    String value,
  ) async {
    await setString('$emailStr/${path.value}', value);
  }

  String getUserData(
    String emailStr,
    UserDataPaths path,
    String value,
  ) {
    return getString('$emailStr/${path.value}') ?? '';
  }

  Future<String?> getSignedInUser() async {
    final signedInUser = getString(signedInUserKey) ?? '';

    return getString(signedInUser);
  }

  Future<void> publishPost(
    String emailStr,
    PostDto postDto,
  ) async {
    final postsJsonList =
        getStringList('$emailStr/${UserDataPaths.posts.value}') ?? [];

    // ignore: cascade_invocations
    postsJsonList.add(jsonEncode(postDto.toJson()));

    await setStringList(
      '$emailStr/${UserDataPaths.posts.value}',
      postsJsonList,
    );
  }

  Future<void> removePost(
    String emailStr,
    PostDto postDto,
  ) async {
    final postsJsonList =
        getStringList('$emailStr/${UserDataPaths.posts.value}') ?? [];

    // ignore: cascade_invocations
    postsJsonList.remove(jsonEncode(postDto.toJson()));
  }

  Future<List<PostDto>> getPosts(
    String emailStr,
  ) async {
    final postsJsonList =
        getStringList('$emailStr/${UserDataPaths.posts.value}') ?? [];

    return postsJsonList
        .map(
          (post) => PostDto.fromJson(jsonDecode(post) as Map<String, dynamic>),
        )
        .toList();
  }

  Future<void> signOut() async {
    await Future.wait([
      remove(signedInUserKey),
    ]);
  }
}
