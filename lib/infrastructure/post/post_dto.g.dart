// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$PostDtoImpl _$$PostDtoImplFromJson(Map<String, dynamic> json) =>
    _$PostDtoImpl(
      userId: json['userId'] as String,
      content: json['content'] as String,
      likesCount: json['likesCount'] as int,
    );

Map<String, dynamic> _$$PostDtoImplToJson(_$PostDtoImpl instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'content': instance.content,
      'likesCount': instance.likesCount,
    };
