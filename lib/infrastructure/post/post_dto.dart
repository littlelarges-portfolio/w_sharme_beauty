// ignore_for_file: invalid_annotation_target, always_put_required_named_parameters_first, lines_longer_than_80_chars

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:w_sharme_beauty/domain/core/value_objects.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/domain/post/post_value_objects.dart';

part 'post_dto.freezed.dart';
part 'post_dto.g.dart';

@freezed
abstract class PostDto implements _$PostDto {
  const factory PostDto({
    @JsonKey(includeFromJson: false) @Default('') String id,
    required String userId,
    required String content,
    required int likesCount,
  }) = _PostDto;

  factory PostDto.fromJson(Map<String, dynamic> json) =>
      _$PostDtoFromJson(json);

  const PostDto._();

  factory PostDto.fromDomain(Post post) {
    return PostDto(
      id: post.id.getOrCrash(),
      userId: post.userId.getOrCrash(),
      content: post.content.getOrCrash(),
      likesCount: post.likesCount,
    );
  }

  Post toDomain() {
    return Post(
      id: UniqueId.fromUniqueString(id),
      userId: UniqueId.fromUniqueString(userId),
      content: Content(content),
      likesCount: likesCount,
    );
  }
}
