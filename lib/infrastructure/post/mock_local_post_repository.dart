import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart';
import 'package:w_sharme_beauty/domain/core/errors/errors.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/domain/post/i_post_repository.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/domain/post/post_failures.dart';
import 'package:w_sharme_beauty/infrastructure/core/mock_local_helpers.dart';
import 'package:w_sharme_beauty/infrastructure/post/post_dto.dart';
import 'package:w_sharme_beauty/injection.dart';

@LazySingleton(as: IPostRepository)
class PostRepository implements IPostRepository {
  PostRepository(this._sharedPreferences);

  final SharedPreferences _sharedPreferences;

  @override
  Future<PostResult> publish(Post post) async {
    try {
      final userOption = await getIt<IAuthFacade>().getSignedInUser();
      final user = userOption.getOrElse(() => throw NotAuthenticatedError());

      final postDto = PostDto.fromDomain(post);

      await _sharedPreferences.publishPost(user.email.getOrCrash(), postDto);

      return right(unit);
    } on PlatformException catch (_) {
      return left(const PostFailure.serverError());
    }
  }

  @override
  Future<PostResult> delete(Post post) async {
    try {
      final userOption = await getIt<IAuthFacade>().getSignedInUser();
      final user = userOption.getOrElse(() => throw NotAuthenticatedError());

      await _sharedPreferences.removePost(
        user.email.getOrCrash(),
        PostDto.fromDomain(post),
      );

      return right(unit);
    } on PlatformException catch (_) {
      return left(const PostFailure.serverError());
    }
  }
}
