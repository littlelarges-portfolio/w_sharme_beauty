// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i3;
import 'package:w_sharme_beauty/application/auth/bloc/auth_bloc.dart' as _i14;
import 'package:w_sharme_beauty/application/auth/login_form/login_form_bloc.dart'
    as _i13;
import 'package:w_sharme_beauty/application/auth/register_form/register_form_bloc.dart'
    as _i12;
import 'package:w_sharme_beauty/application/post/post_fetcher/posts_fetcher_bloc.dart'
    as _i11;
import 'package:w_sharme_beauty/application/post/post_form_bloc.dart' as _i10;
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart' as _i8;
import 'package:w_sharme_beauty/domain/post/i_post_repository.dart' as _i6;
import 'package:w_sharme_beauty/domain/user_actions/i_user_actions_repository.dart'
    as _i4;
import 'package:w_sharme_beauty/infrastructure/auth/mock_local_auth_facade.dart'
    as _i9;
import 'package:w_sharme_beauty/infrastructure/core/core_injectable_module.dart'
    as _i15;
import 'package:w_sharme_beauty/infrastructure/post/mock_local_post_repository.dart'
    as _i7;
import 'package:w_sharme_beauty/infrastructure/user_actions/mock_local_user_actions_repository.dart'
    as _i5;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final coreInjectableModule = _$CoreInjectableModule();
    await gh.factoryAsync<_i3.SharedPreferences>(
      () => coreInjectableModule.sharedPreferences,
      preResolve: true,
    );
    gh.lazySingleton<_i4.IUserActionsRepository>(
        () => _i5.UserActionsRepository(gh<_i3.SharedPreferences>()));
    gh.lazySingleton<_i6.IPostRepository>(
        () => _i7.PostRepository(gh<_i3.SharedPreferences>()));
    gh.lazySingleton<_i8.IAuthFacade>(
        () => _i9.MockLocalAuthFacade(gh<_i3.SharedPreferences>()));
    gh.factory<_i10.PostFormBloc>(
        () => _i10.PostFormBloc(gh<_i6.IPostRepository>()));
    gh.factory<_i11.PostsFetcherBloc>(
        () => _i11.PostsFetcherBloc(gh<_i4.IUserActionsRepository>()));
    gh.factory<_i12.RegisterFormBloc>(
        () => _i12.RegisterFormBloc(gh<_i8.IAuthFacade>()));
    gh.factory<_i13.LoginFormBloc>(
        () => _i13.LoginFormBloc(gh<_i8.IAuthFacade>()));
    gh.factory<_i14.AuthBloc>(() => _i14.AuthBloc(gh<_i8.IAuthFacade>()));
    return this;
  }
}

class _$CoreInjectableModule extends _i15.CoreInjectableModule {}
