import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:w_sharme_beauty/domain/auth/auth_failures.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';

part 'login_form_event.dart';
part 'login_form_state.dart';
part 'login_form_bloc.freezed.dart';

@injectable
class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState> {
  LoginFormBloc(this._authFacade) : super(LoginFormState.initial()) {
    on<LoginFormEvent>((event, emit) async {
      await event.map(
        emailChanged: (e) {
          emit(
            state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              loginResultOption: none(),
            ),
          );
        },
        passwordChanged: (e) {
          emit(
            state.copyWith(
              password: Password(e.passwordStr),
              loginResultOption: none(),
            ),
          );
        },
        loginPressed: (e) async {
          AuthResult? failureOrSuccess;

          final isEmailValid = state.emailAddress.isValid();
          final isPassordValid = state.password.isValid();

          if (isEmailValid && isPassordValid) {
            emit(
              state.copyWith(
                isSubmiting: true,
                loginResultOption: none(),
              ),
            );

            failureOrSuccess = await _authFacade.login(
              email: state.emailAddress,
              password: state.password,
            );
          }

          emit(
            state.copyWith(
              isSubmiting: false,
              showErrorMessages: AutovalidateMode.always,
              loginResultOption: optionOf(failureOrSuccess),
            ),
          );
        },
      );
    });
  }

  final IAuthFacade _authFacade;
}
