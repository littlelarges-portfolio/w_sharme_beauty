import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:w_sharme_beauty/domain/auth/auth_failures.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/auth/i_auth_facade.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';

part 'register_form_event.dart';
part 'register_form_state.dart';
part 'register_form_bloc.freezed.dart';

@injectable
class RegisterFormBloc extends Bloc<RegisterFormEvent, RegisterFormState> {
  RegisterFormBloc(this._authFacade) : super(RegisterFormState.initial()) {
    on<RegisterFormEvent>((event, emit) async {
      await event.map(
        emailChanged: (e) {
          emit(
            state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              registerResultOption: none(),
            ),
          );
        },
        passwordChanged: (e) {
          emit(
            state.copyWith(
              password: Password(e.passwordStr),
              registerResultOption: none(),
            ),
          );
        },
        fullnameChanged: (e) {
          emit(
            state.copyWith(
              fullname: Fullname(e.fullnameStr),
              registerResultOption: none(),
            ),
          );
        },
        usernameChanged: (e) {
          emit(
            state.copyWith(
              username: Username(e.usernameStr),
              registerResultOption: none(),
            ),
          );
        },
        citynameChanged: (e) {
          emit(
            state.copyWith(
              cityname: Cityname(e.citynameStr),
              registerResultOption: none(),
            ),
          );
        },
        continuePressed: (e) async {
          AuthResult? failureOrSuccess;

          final isEmailValid = state.emailAddress.isValid();
          final isPassordValid = state.password.isValid();

          if (isEmailValid && isPassordValid) {
            emit(state.copyWith(registerResultOption: none()));

            failureOrSuccess = await _authFacade.register(
              email: state.emailAddress,
              password: state.password,
            );
          }

          emit(
            state.copyWith(
              showErrorMessages: AutovalidateMode.always,
              registerResultOption: optionOf(failureOrSuccess),
            ),
          );
        },
        saveUserDataPressed: (e) async {
          AuthResult? failureOrSuccess;

          final isFullnameValid = state.fullname.isValid();
          final isUsernameValid = state.username.isValid();
          final isCitynameValid = state.cityname.isValid();

          if (isFullnameValid && isUsernameValid && isCitynameValid) {
            emit(state.copyWith(registerResultOption: none()));

            final signedInUserOption = await _authFacade.getSignedInUser();

            final userEmail = signedInUserOption
                .getOrElse(() => throw UnimplementedError())
                .email;

            failureOrSuccess = await _authFacade.saveUserData(
              email: userEmail,
              fullname: state.fullname,
              username: state.username,
              cityname: state.cityname,
            );
          }

          emit(
            state.copyWith(
              showErrorMessages: AutovalidateMode.always,
              registerResultOption: optionOf(failureOrSuccess),
            ),
          );
        },
      );
    });
  }

  final IAuthFacade _authFacade;
}
