// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'register_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$RegisterFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterFormEventCopyWith<$Res> {
  factory $RegisterFormEventCopyWith(
          RegisterFormEvent value, $Res Function(RegisterFormEvent) then) =
      _$RegisterFormEventCopyWithImpl<$Res, RegisterFormEvent>;
}

/// @nodoc
class _$RegisterFormEventCopyWithImpl<$Res, $Val extends RegisterFormEvent>
    implements $RegisterFormEventCopyWith<$Res> {
  _$RegisterFormEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$EmailChangedImplCopyWith<$Res> {
  factory _$$EmailChangedImplCopyWith(
          _$EmailChangedImpl value, $Res Function(_$EmailChangedImpl) then) =
      __$$EmailChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String emailStr});
}

/// @nodoc
class __$$EmailChangedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$EmailChangedImpl>
    implements _$$EmailChangedImplCopyWith<$Res> {
  __$$EmailChangedImplCopyWithImpl(
      _$EmailChangedImpl _value, $Res Function(_$EmailChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailStr = null,
  }) {
    return _then(_$EmailChangedImpl(
      null == emailStr
          ? _value.emailStr
          : emailStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$EmailChangedImpl implements _EmailChanged {
  const _$EmailChangedImpl(this.emailStr);

  @override
  final String emailStr;

  @override
  String toString() {
    return 'RegisterFormEvent.emailChanged(emailStr: $emailStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EmailChangedImpl &&
            (identical(other.emailStr, emailStr) ||
                other.emailStr == emailStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EmailChangedImplCopyWith<_$EmailChangedImpl> get copyWith =>
      __$$EmailChangedImplCopyWithImpl<_$EmailChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return emailChanged(emailStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return emailChanged?.call(emailStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(emailStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return emailChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return emailChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (emailChanged != null) {
      return emailChanged(this);
    }
    return orElse();
  }
}

abstract class _EmailChanged implements RegisterFormEvent {
  const factory _EmailChanged(final String emailStr) = _$EmailChangedImpl;

  String get emailStr;
  @JsonKey(ignore: true)
  _$$EmailChangedImplCopyWith<_$EmailChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PasswordChangedImplCopyWith<$Res> {
  factory _$$PasswordChangedImplCopyWith(_$PasswordChangedImpl value,
          $Res Function(_$PasswordChangedImpl) then) =
      __$$PasswordChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String passwordStr});
}

/// @nodoc
class __$$PasswordChangedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$PasswordChangedImpl>
    implements _$$PasswordChangedImplCopyWith<$Res> {
  __$$PasswordChangedImplCopyWithImpl(
      _$PasswordChangedImpl _value, $Res Function(_$PasswordChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? passwordStr = null,
  }) {
    return _then(_$PasswordChangedImpl(
      null == passwordStr
          ? _value.passwordStr
          : passwordStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PasswordChangedImpl implements _PasswordChanged {
  const _$PasswordChangedImpl(this.passwordStr);

  @override
  final String passwordStr;

  @override
  String toString() {
    return 'RegisterFormEvent.passwordChanged(passwordStr: $passwordStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PasswordChangedImpl &&
            (identical(other.passwordStr, passwordStr) ||
                other.passwordStr == passwordStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, passwordStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PasswordChangedImplCopyWith<_$PasswordChangedImpl> get copyWith =>
      __$$PasswordChangedImplCopyWithImpl<_$PasswordChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return passwordChanged(passwordStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return passwordChanged?.call(passwordStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (passwordChanged != null) {
      return passwordChanged(passwordStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return passwordChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return passwordChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (passwordChanged != null) {
      return passwordChanged(this);
    }
    return orElse();
  }
}

abstract class _PasswordChanged implements RegisterFormEvent {
  const factory _PasswordChanged(final String passwordStr) =
      _$PasswordChangedImpl;

  String get passwordStr;
  @JsonKey(ignore: true)
  _$$PasswordChangedImplCopyWith<_$PasswordChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FullnameChangedImplCopyWith<$Res> {
  factory _$$FullnameChangedImplCopyWith(_$FullnameChangedImpl value,
          $Res Function(_$FullnameChangedImpl) then) =
      __$$FullnameChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String fullnameStr});
}

/// @nodoc
class __$$FullnameChangedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$FullnameChangedImpl>
    implements _$$FullnameChangedImplCopyWith<$Res> {
  __$$FullnameChangedImplCopyWithImpl(
      _$FullnameChangedImpl _value, $Res Function(_$FullnameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? fullnameStr = null,
  }) {
    return _then(_$FullnameChangedImpl(
      null == fullnameStr
          ? _value.fullnameStr
          : fullnameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FullnameChangedImpl implements _FullnameChanged {
  const _$FullnameChangedImpl(this.fullnameStr);

  @override
  final String fullnameStr;

  @override
  String toString() {
    return 'RegisterFormEvent.fullnameChanged(fullnameStr: $fullnameStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FullnameChangedImpl &&
            (identical(other.fullnameStr, fullnameStr) ||
                other.fullnameStr == fullnameStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, fullnameStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FullnameChangedImplCopyWith<_$FullnameChangedImpl> get copyWith =>
      __$$FullnameChangedImplCopyWithImpl<_$FullnameChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return fullnameChanged(fullnameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return fullnameChanged?.call(fullnameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (fullnameChanged != null) {
      return fullnameChanged(fullnameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return fullnameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return fullnameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (fullnameChanged != null) {
      return fullnameChanged(this);
    }
    return orElse();
  }
}

abstract class _FullnameChanged implements RegisterFormEvent {
  const factory _FullnameChanged(final String fullnameStr) =
      _$FullnameChangedImpl;

  String get fullnameStr;
  @JsonKey(ignore: true)
  _$$FullnameChangedImplCopyWith<_$FullnameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UsernameChangedImplCopyWith<$Res> {
  factory _$$UsernameChangedImplCopyWith(_$UsernameChangedImpl value,
          $Res Function(_$UsernameChangedImpl) then) =
      __$$UsernameChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String usernameStr});
}

/// @nodoc
class __$$UsernameChangedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$UsernameChangedImpl>
    implements _$$UsernameChangedImplCopyWith<$Res> {
  __$$UsernameChangedImplCopyWithImpl(
      _$UsernameChangedImpl _value, $Res Function(_$UsernameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? usernameStr = null,
  }) {
    return _then(_$UsernameChangedImpl(
      null == usernameStr
          ? _value.usernameStr
          : usernameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UsernameChangedImpl implements _UsernameChanged {
  const _$UsernameChangedImpl(this.usernameStr);

  @override
  final String usernameStr;

  @override
  String toString() {
    return 'RegisterFormEvent.usernameChanged(usernameStr: $usernameStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UsernameChangedImpl &&
            (identical(other.usernameStr, usernameStr) ||
                other.usernameStr == usernameStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, usernameStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UsernameChangedImplCopyWith<_$UsernameChangedImpl> get copyWith =>
      __$$UsernameChangedImplCopyWithImpl<_$UsernameChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return usernameChanged(usernameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return usernameChanged?.call(usernameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (usernameChanged != null) {
      return usernameChanged(usernameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return usernameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return usernameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (usernameChanged != null) {
      return usernameChanged(this);
    }
    return orElse();
  }
}

abstract class _UsernameChanged implements RegisterFormEvent {
  const factory _UsernameChanged(final String usernameStr) =
      _$UsernameChangedImpl;

  String get usernameStr;
  @JsonKey(ignore: true)
  _$$UsernameChangedImplCopyWith<_$UsernameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CitynameChangedImplCopyWith<$Res> {
  factory _$$CitynameChangedImplCopyWith(_$CitynameChangedImpl value,
          $Res Function(_$CitynameChangedImpl) then) =
      __$$CitynameChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String citynameStr});
}

/// @nodoc
class __$$CitynameChangedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$CitynameChangedImpl>
    implements _$$CitynameChangedImplCopyWith<$Res> {
  __$$CitynameChangedImplCopyWithImpl(
      _$CitynameChangedImpl _value, $Res Function(_$CitynameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? citynameStr = null,
  }) {
    return _then(_$CitynameChangedImpl(
      null == citynameStr
          ? _value.citynameStr
          : citynameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CitynameChangedImpl implements _CitynameChanged {
  const _$CitynameChangedImpl(this.citynameStr);

  @override
  final String citynameStr;

  @override
  String toString() {
    return 'RegisterFormEvent.citynameChanged(citynameStr: $citynameStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CitynameChangedImpl &&
            (identical(other.citynameStr, citynameStr) ||
                other.citynameStr == citynameStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, citynameStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CitynameChangedImplCopyWith<_$CitynameChangedImpl> get copyWith =>
      __$$CitynameChangedImplCopyWithImpl<_$CitynameChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return citynameChanged(citynameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return citynameChanged?.call(citynameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (citynameChanged != null) {
      return citynameChanged(citynameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return citynameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return citynameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (citynameChanged != null) {
      return citynameChanged(this);
    }
    return orElse();
  }
}

abstract class _CitynameChanged implements RegisterFormEvent {
  const factory _CitynameChanged(final String citynameStr) =
      _$CitynameChangedImpl;

  String get citynameStr;
  @JsonKey(ignore: true)
  _$$CitynameChangedImplCopyWith<_$CitynameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RegisterPressedImplCopyWith<$Res> {
  factory _$$RegisterPressedImplCopyWith(_$RegisterPressedImpl value,
          $Res Function(_$RegisterPressedImpl) then) =
      __$$RegisterPressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RegisterPressedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$RegisterPressedImpl>
    implements _$$RegisterPressedImplCopyWith<$Res> {
  __$$RegisterPressedImplCopyWithImpl(
      _$RegisterPressedImpl _value, $Res Function(_$RegisterPressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RegisterPressedImpl implements _RegisterPressed {
  const _$RegisterPressedImpl();

  @override
  String toString() {
    return 'RegisterFormEvent.continuePressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$RegisterPressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return continuePressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return continuePressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (continuePressed != null) {
      return continuePressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return continuePressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return continuePressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (continuePressed != null) {
      return continuePressed(this);
    }
    return orElse();
  }
}

abstract class _RegisterPressed implements RegisterFormEvent {
  const factory _RegisterPressed() = _$RegisterPressedImpl;
}

/// @nodoc
abstract class _$$ContinuePressedImplCopyWith<$Res> {
  factory _$$ContinuePressedImplCopyWith(_$ContinuePressedImpl value,
          $Res Function(_$ContinuePressedImpl) then) =
      __$$ContinuePressedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ContinuePressedImplCopyWithImpl<$Res>
    extends _$RegisterFormEventCopyWithImpl<$Res, _$ContinuePressedImpl>
    implements _$$ContinuePressedImplCopyWith<$Res> {
  __$$ContinuePressedImplCopyWithImpl(
      _$ContinuePressedImpl _value, $Res Function(_$ContinuePressedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ContinuePressedImpl implements _ContinuePressed {
  const _$ContinuePressedImpl();

  @override
  String toString() {
    return 'RegisterFormEvent.saveUserDataPressed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ContinuePressedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String emailStr) emailChanged,
    required TResult Function(String passwordStr) passwordChanged,
    required TResult Function(String fullnameStr) fullnameChanged,
    required TResult Function(String usernameStr) usernameChanged,
    required TResult Function(String citynameStr) citynameChanged,
    required TResult Function() continuePressed,
    required TResult Function() saveUserDataPressed,
  }) {
    return saveUserDataPressed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String emailStr)? emailChanged,
    TResult? Function(String passwordStr)? passwordChanged,
    TResult? Function(String fullnameStr)? fullnameChanged,
    TResult? Function(String usernameStr)? usernameChanged,
    TResult? Function(String citynameStr)? citynameChanged,
    TResult? Function()? continuePressed,
    TResult? Function()? saveUserDataPressed,
  }) {
    return saveUserDataPressed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String emailStr)? emailChanged,
    TResult Function(String passwordStr)? passwordChanged,
    TResult Function(String fullnameStr)? fullnameChanged,
    TResult Function(String usernameStr)? usernameChanged,
    TResult Function(String citynameStr)? citynameChanged,
    TResult Function()? continuePressed,
    TResult Function()? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (saveUserDataPressed != null) {
      return saveUserDataPressed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_EmailChanged value) emailChanged,
    required TResult Function(_PasswordChanged value) passwordChanged,
    required TResult Function(_FullnameChanged value) fullnameChanged,
    required TResult Function(_UsernameChanged value) usernameChanged,
    required TResult Function(_CitynameChanged value) citynameChanged,
    required TResult Function(_RegisterPressed value) continuePressed,
    required TResult Function(_ContinuePressed value) saveUserDataPressed,
  }) {
    return saveUserDataPressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_EmailChanged value)? emailChanged,
    TResult? Function(_PasswordChanged value)? passwordChanged,
    TResult? Function(_FullnameChanged value)? fullnameChanged,
    TResult? Function(_UsernameChanged value)? usernameChanged,
    TResult? Function(_CitynameChanged value)? citynameChanged,
    TResult? Function(_RegisterPressed value)? continuePressed,
    TResult? Function(_ContinuePressed value)? saveUserDataPressed,
  }) {
    return saveUserDataPressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_EmailChanged value)? emailChanged,
    TResult Function(_PasswordChanged value)? passwordChanged,
    TResult Function(_FullnameChanged value)? fullnameChanged,
    TResult Function(_UsernameChanged value)? usernameChanged,
    TResult Function(_CitynameChanged value)? citynameChanged,
    TResult Function(_RegisterPressed value)? continuePressed,
    TResult Function(_ContinuePressed value)? saveUserDataPressed,
    required TResult orElse(),
  }) {
    if (saveUserDataPressed != null) {
      return saveUserDataPressed(this);
    }
    return orElse();
  }
}

abstract class _ContinuePressed implements RegisterFormEvent {
  const factory _ContinuePressed() = _$ContinuePressedImpl;
}

/// @nodoc
mixin _$RegisterFormState {
  EmailAddress get emailAddress => throw _privateConstructorUsedError;
  Password get password => throw _privateConstructorUsedError;
  Fullname get fullname => throw _privateConstructorUsedError;
  Username get username => throw _privateConstructorUsedError;
  Cityname get cityname => throw _privateConstructorUsedError;
  AutovalidateMode get showErrorMessages => throw _privateConstructorUsedError;
  Option<Either<AuthFailure, Unit>> get registerResultOption =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RegisterFormStateCopyWith<RegisterFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterFormStateCopyWith<$Res> {
  factory $RegisterFormStateCopyWith(
          RegisterFormState value, $Res Function(RegisterFormState) then) =
      _$RegisterFormStateCopyWithImpl<$Res, RegisterFormState>;
  @useResult
  $Res call(
      {EmailAddress emailAddress,
      Password password,
      Fullname fullname,
      Username username,
      Cityname cityname,
      AutovalidateMode showErrorMessages,
      Option<Either<AuthFailure, Unit>> registerResultOption});
}

/// @nodoc
class _$RegisterFormStateCopyWithImpl<$Res, $Val extends RegisterFormState>
    implements $RegisterFormStateCopyWith<$Res> {
  _$RegisterFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailAddress = null,
    Object? password = null,
    Object? fullname = null,
    Object? username = null,
    Object? cityname = null,
    Object? showErrorMessages = null,
    Object? registerResultOption = null,
  }) {
    return _then(_value.copyWith(
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as Password,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as Fullname,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as Username,
      cityname: null == cityname
          ? _value.cityname
          : cityname // ignore: cast_nullable_to_non_nullable
              as Cityname,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      registerResultOption: null == registerResultOption
          ? _value.registerResultOption
          : registerResultOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<AuthFailure, Unit>>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RegisterFormStateImplCopyWith<$Res>
    implements $RegisterFormStateCopyWith<$Res> {
  factory _$$RegisterFormStateImplCopyWith(_$RegisterFormStateImpl value,
          $Res Function(_$RegisterFormStateImpl) then) =
      __$$RegisterFormStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {EmailAddress emailAddress,
      Password password,
      Fullname fullname,
      Username username,
      Cityname cityname,
      AutovalidateMode showErrorMessages,
      Option<Either<AuthFailure, Unit>> registerResultOption});
}

/// @nodoc
class __$$RegisterFormStateImplCopyWithImpl<$Res>
    extends _$RegisterFormStateCopyWithImpl<$Res, _$RegisterFormStateImpl>
    implements _$$RegisterFormStateImplCopyWith<$Res> {
  __$$RegisterFormStateImplCopyWithImpl(_$RegisterFormStateImpl _value,
      $Res Function(_$RegisterFormStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailAddress = null,
    Object? password = null,
    Object? fullname = null,
    Object? username = null,
    Object? cityname = null,
    Object? showErrorMessages = null,
    Object? registerResultOption = null,
  }) {
    return _then(_$RegisterFormStateImpl(
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as Password,
      fullname: null == fullname
          ? _value.fullname
          : fullname // ignore: cast_nullable_to_non_nullable
              as Fullname,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as Username,
      cityname: null == cityname
          ? _value.cityname
          : cityname // ignore: cast_nullable_to_non_nullable
              as Cityname,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      registerResultOption: null == registerResultOption
          ? _value.registerResultOption
          : registerResultOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<AuthFailure, Unit>>,
    ));
  }
}

/// @nodoc

class _$RegisterFormStateImpl implements _RegisterFormState {
  const _$RegisterFormStateImpl(
      {required this.emailAddress,
      required this.password,
      required this.fullname,
      required this.username,
      required this.cityname,
      required this.showErrorMessages,
      required this.registerResultOption});

  @override
  final EmailAddress emailAddress;
  @override
  final Password password;
  @override
  final Fullname fullname;
  @override
  final Username username;
  @override
  final Cityname cityname;
  @override
  final AutovalidateMode showErrorMessages;
  @override
  final Option<Either<AuthFailure, Unit>> registerResultOption;

  @override
  String toString() {
    return 'RegisterFormState(emailAddress: $emailAddress, password: $password, fullname: $fullname, username: $username, cityname: $cityname, showErrorMessages: $showErrorMessages, registerResultOption: $registerResultOption)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RegisterFormStateImpl &&
            (identical(other.emailAddress, emailAddress) ||
                other.emailAddress == emailAddress) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.fullname, fullname) ||
                other.fullname == fullname) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.cityname, cityname) ||
                other.cityname == cityname) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                other.showErrorMessages == showErrorMessages) &&
            (identical(other.registerResultOption, registerResultOption) ||
                other.registerResultOption == registerResultOption));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailAddress, password, fullname,
      username, cityname, showErrorMessages, registerResultOption);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RegisterFormStateImplCopyWith<_$RegisterFormStateImpl> get copyWith =>
      __$$RegisterFormStateImplCopyWithImpl<_$RegisterFormStateImpl>(
          this, _$identity);
}

abstract class _RegisterFormState implements RegisterFormState {
  const factory _RegisterFormState(
      {required final EmailAddress emailAddress,
      required final Password password,
      required final Fullname fullname,
      required final Username username,
      required final Cityname cityname,
      required final AutovalidateMode showErrorMessages,
      required final Option<Either<AuthFailure, Unit>>
          registerResultOption}) = _$RegisterFormStateImpl;

  @override
  EmailAddress get emailAddress;
  @override
  Password get password;
  @override
  Fullname get fullname;
  @override
  Username get username;
  @override
  Cityname get cityname;
  @override
  AutovalidateMode get showErrorMessages;
  @override
  Option<Either<AuthFailure, Unit>> get registerResultOption;
  @override
  @JsonKey(ignore: true)
  _$$RegisterFormStateImplCopyWith<_$RegisterFormStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
