part of 'register_form_bloc.dart';

@freezed
class RegisterFormEvent with _$RegisterFormEvent {
  const factory RegisterFormEvent.emailChanged(String emailStr) = _EmailChanged;
  const factory RegisterFormEvent.passwordChanged(String passwordStr) =
      _PasswordChanged;
  const factory RegisterFormEvent.fullnameChanged(String fullnameStr) =
      _FullnameChanged;
  const factory RegisterFormEvent.usernameChanged(String usernameStr) =
      _UsernameChanged;
  const factory RegisterFormEvent.citynameChanged(String citynameStr) =
      _CitynameChanged;
  const factory RegisterFormEvent.continuePressed() = _RegisterPressed;
  const factory RegisterFormEvent.saveUserDataPressed() = _ContinuePressed;
}
