part of 'register_form_bloc.dart';

@freezed
class RegisterFormState with _$RegisterFormState {
  const factory RegisterFormState({
    required EmailAddress emailAddress,
    required Password password,
    required Fullname fullname,
    required Username username,
    required Cityname cityname,
    required AutovalidateMode showErrorMessages,
    required AuthResultOption registerResultOption,
  }) = _RegisterFormState;

  factory RegisterFormState.initial() => RegisterFormState(
        emailAddress: EmailAddress(''),
        password: Password(''),
        fullname: Fullname(''),
        username: Username(''),
        cityname: Cityname(''),
        showErrorMessages: AutovalidateMode.disabled,
        registerResultOption: none(),
      );
}
