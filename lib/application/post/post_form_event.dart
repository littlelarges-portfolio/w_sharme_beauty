part of 'post_form_bloc.dart';

@freezed
class PostFormEvent with _$PostFormEvent {
  const factory PostFormEvent.contentChanged(String contentStr) =
      _ContentChanged;
  const factory PostFormEvent.published() = _Published;
}
