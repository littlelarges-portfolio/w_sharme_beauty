part of 'posts_fetcher_bloc.dart';

@freezed
class PostsFetcherEvent with _$PostsFetcherEvent {
  const factory PostsFetcherEvent.fetched({
    required EmailAddress emailAddress,
  }) = _Fetched;
}
