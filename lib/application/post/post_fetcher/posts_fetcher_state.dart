part of 'posts_fetcher_bloc.dart';

@freezed
class PostsFetcherState with _$PostsFetcherState {
  const factory PostsFetcherState.initial() = _Initial;
  const factory PostsFetcherState.fetchingInProgress() = _FetchingInProgress;
  const factory PostsFetcherState.fetchingSuccess({
    required KtList<Post> posts,
  }) = _FetchingSuccess;
  const factory PostsFetcherState.fetchingFailed({
    required UserActionsFailure userActionsFailure,
  }) = _FetchingFailed;
}
