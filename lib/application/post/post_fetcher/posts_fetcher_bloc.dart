import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:w_sharme_beauty/domain/auth/auth_value_objects.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/domain/user_actions/i_user_actions_repository.dart';
import 'package:w_sharme_beauty/domain/user_actions/user_actions_failures.dart';

part 'posts_fetcher_event.dart';
part 'posts_fetcher_state.dart';
part 'posts_fetcher_bloc.freezed.dart';

@injectable
class PostsFetcherBloc extends Bloc<PostsFetcherEvent, PostsFetcherState> {
  PostsFetcherBloc(this._userActionsRepository)
      : super(const PostsFetcherState.initial()) {
    on<PostsFetcherEvent>((event, emit) async {
      await event.map(
        fetched: (e) async {
          emit(const PostsFetcherState.fetchingInProgress());

          final possibleFailure =
              await _userActionsRepository.fetchPosts(email: e.emailAddress);

          emit(
            possibleFailure.fold(
              (f) => PostsFetcherState.fetchingFailed(userActionsFailure: f),
              (posts) => PostsFetcherState.fetchingSuccess(posts: posts),
            ),
          );
        },
      );
    });
  }

  final IUserActionsRepository _userActionsRepository;
}
