// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'posts_fetcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PostsFetcherEvent {
  EmailAddress get emailAddress => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(EmailAddress emailAddress) fetched,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(EmailAddress emailAddress)? fetched,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(EmailAddress emailAddress)? fetched,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Fetched value) fetched,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Fetched value)? fetched,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Fetched value)? fetched,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PostsFetcherEventCopyWith<PostsFetcherEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostsFetcherEventCopyWith<$Res> {
  factory $PostsFetcherEventCopyWith(
          PostsFetcherEvent value, $Res Function(PostsFetcherEvent) then) =
      _$PostsFetcherEventCopyWithImpl<$Res, PostsFetcherEvent>;
  @useResult
  $Res call({EmailAddress emailAddress});
}

/// @nodoc
class _$PostsFetcherEventCopyWithImpl<$Res, $Val extends PostsFetcherEvent>
    implements $PostsFetcherEventCopyWith<$Res> {
  _$PostsFetcherEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailAddress = null,
  }) {
    return _then(_value.copyWith(
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FetchedImplCopyWith<$Res>
    implements $PostsFetcherEventCopyWith<$Res> {
  factory _$$FetchedImplCopyWith(
          _$FetchedImpl value, $Res Function(_$FetchedImpl) then) =
      __$$FetchedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({EmailAddress emailAddress});
}

/// @nodoc
class __$$FetchedImplCopyWithImpl<$Res>
    extends _$PostsFetcherEventCopyWithImpl<$Res, _$FetchedImpl>
    implements _$$FetchedImplCopyWith<$Res> {
  __$$FetchedImplCopyWithImpl(
      _$FetchedImpl _value, $Res Function(_$FetchedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? emailAddress = null,
  }) {
    return _then(_$FetchedImpl(
      emailAddress: null == emailAddress
          ? _value.emailAddress
          : emailAddress // ignore: cast_nullable_to_non_nullable
              as EmailAddress,
    ));
  }
}

/// @nodoc

class _$FetchedImpl implements _Fetched {
  const _$FetchedImpl({required this.emailAddress});

  @override
  final EmailAddress emailAddress;

  @override
  String toString() {
    return 'PostsFetcherEvent.fetched(emailAddress: $emailAddress)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchedImpl &&
            (identical(other.emailAddress, emailAddress) ||
                other.emailAddress == emailAddress));
  }

  @override
  int get hashCode => Object.hash(runtimeType, emailAddress);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchedImplCopyWith<_$FetchedImpl> get copyWith =>
      __$$FetchedImplCopyWithImpl<_$FetchedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(EmailAddress emailAddress) fetched,
  }) {
    return fetched(emailAddress);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(EmailAddress emailAddress)? fetched,
  }) {
    return fetched?.call(emailAddress);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(EmailAddress emailAddress)? fetched,
    required TResult orElse(),
  }) {
    if (fetched != null) {
      return fetched(emailAddress);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Fetched value) fetched,
  }) {
    return fetched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Fetched value)? fetched,
  }) {
    return fetched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Fetched value)? fetched,
    required TResult orElse(),
  }) {
    if (fetched != null) {
      return fetched(this);
    }
    return orElse();
  }
}

abstract class _Fetched implements PostsFetcherEvent {
  const factory _Fetched({required final EmailAddress emailAddress}) =
      _$FetchedImpl;

  @override
  EmailAddress get emailAddress;
  @override
  @JsonKey(ignore: true)
  _$$FetchedImplCopyWith<_$FetchedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$PostsFetcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingInProgress,
    required TResult Function(KtList<Post> posts) fetchingSuccess,
    required TResult Function(UserActionsFailure userActionsFailure)
        fetchingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fetchingInProgress,
    TResult? Function(KtList<Post> posts)? fetchingSuccess,
    TResult? Function(UserActionsFailure userActionsFailure)? fetchingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingInProgress,
    TResult Function(KtList<Post> posts)? fetchingSuccess,
    TResult Function(UserActionsFailure userActionsFailure)? fetchingFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FetchingInProgress value) fetchingInProgress,
    required TResult Function(_FetchingSuccess value) fetchingSuccess,
    required TResult Function(_FetchingFailed value) fetchingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_FetchingInProgress value)? fetchingInProgress,
    TResult? Function(_FetchingSuccess value)? fetchingSuccess,
    TResult? Function(_FetchingFailed value)? fetchingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FetchingInProgress value)? fetchingInProgress,
    TResult Function(_FetchingSuccess value)? fetchingSuccess,
    TResult Function(_FetchingFailed value)? fetchingFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostsFetcherStateCopyWith<$Res> {
  factory $PostsFetcherStateCopyWith(
          PostsFetcherState value, $Res Function(PostsFetcherState) then) =
      _$PostsFetcherStateCopyWithImpl<$Res, PostsFetcherState>;
}

/// @nodoc
class _$PostsFetcherStateCopyWithImpl<$Res, $Val extends PostsFetcherState>
    implements $PostsFetcherStateCopyWith<$Res> {
  _$PostsFetcherStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$PostsFetcherStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'PostsFetcherState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingInProgress,
    required TResult Function(KtList<Post> posts) fetchingSuccess,
    required TResult Function(UserActionsFailure userActionsFailure)
        fetchingFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fetchingInProgress,
    TResult? Function(KtList<Post> posts)? fetchingSuccess,
    TResult? Function(UserActionsFailure userActionsFailure)? fetchingFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingInProgress,
    TResult Function(KtList<Post> posts)? fetchingSuccess,
    TResult Function(UserActionsFailure userActionsFailure)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FetchingInProgress value) fetchingInProgress,
    required TResult Function(_FetchingSuccess value) fetchingSuccess,
    required TResult Function(_FetchingFailed value) fetchingFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_FetchingInProgress value)? fetchingInProgress,
    TResult? Function(_FetchingSuccess value)? fetchingSuccess,
    TResult? Function(_FetchingFailed value)? fetchingFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FetchingInProgress value)? fetchingInProgress,
    TResult Function(_FetchingSuccess value)? fetchingSuccess,
    TResult Function(_FetchingFailed value)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements PostsFetcherState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$FetchingInProgressImplCopyWith<$Res> {
  factory _$$FetchingInProgressImplCopyWith(_$FetchingInProgressImpl value,
          $Res Function(_$FetchingInProgressImpl) then) =
      __$$FetchingInProgressImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchingInProgressImplCopyWithImpl<$Res>
    extends _$PostsFetcherStateCopyWithImpl<$Res, _$FetchingInProgressImpl>
    implements _$$FetchingInProgressImplCopyWith<$Res> {
  __$$FetchingInProgressImplCopyWithImpl(_$FetchingInProgressImpl _value,
      $Res Function(_$FetchingInProgressImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FetchingInProgressImpl implements _FetchingInProgress {
  const _$FetchingInProgressImpl();

  @override
  String toString() {
    return 'PostsFetcherState.fetchingInProgress()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchingInProgressImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingInProgress,
    required TResult Function(KtList<Post> posts) fetchingSuccess,
    required TResult Function(UserActionsFailure userActionsFailure)
        fetchingFailed,
  }) {
    return fetchingInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fetchingInProgress,
    TResult? Function(KtList<Post> posts)? fetchingSuccess,
    TResult? Function(UserActionsFailure userActionsFailure)? fetchingFailed,
  }) {
    return fetchingInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingInProgress,
    TResult Function(KtList<Post> posts)? fetchingSuccess,
    TResult Function(UserActionsFailure userActionsFailure)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingInProgress != null) {
      return fetchingInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FetchingInProgress value) fetchingInProgress,
    required TResult Function(_FetchingSuccess value) fetchingSuccess,
    required TResult Function(_FetchingFailed value) fetchingFailed,
  }) {
    return fetchingInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_FetchingInProgress value)? fetchingInProgress,
    TResult? Function(_FetchingSuccess value)? fetchingSuccess,
    TResult? Function(_FetchingFailed value)? fetchingFailed,
  }) {
    return fetchingInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FetchingInProgress value)? fetchingInProgress,
    TResult Function(_FetchingSuccess value)? fetchingSuccess,
    TResult Function(_FetchingFailed value)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingInProgress != null) {
      return fetchingInProgress(this);
    }
    return orElse();
  }
}

abstract class _FetchingInProgress implements PostsFetcherState {
  const factory _FetchingInProgress() = _$FetchingInProgressImpl;
}

/// @nodoc
abstract class _$$FetchingSuccessImplCopyWith<$Res> {
  factory _$$FetchingSuccessImplCopyWith(_$FetchingSuccessImpl value,
          $Res Function(_$FetchingSuccessImpl) then) =
      __$$FetchingSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({KtList<Post> posts});
}

/// @nodoc
class __$$FetchingSuccessImplCopyWithImpl<$Res>
    extends _$PostsFetcherStateCopyWithImpl<$Res, _$FetchingSuccessImpl>
    implements _$$FetchingSuccessImplCopyWith<$Res> {
  __$$FetchingSuccessImplCopyWithImpl(
      _$FetchingSuccessImpl _value, $Res Function(_$FetchingSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? posts = null,
  }) {
    return _then(_$FetchingSuccessImpl(
      posts: null == posts
          ? _value.posts
          : posts // ignore: cast_nullable_to_non_nullable
              as KtList<Post>,
    ));
  }
}

/// @nodoc

class _$FetchingSuccessImpl implements _FetchingSuccess {
  const _$FetchingSuccessImpl({required this.posts});

  @override
  final KtList<Post> posts;

  @override
  String toString() {
    return 'PostsFetcherState.fetchingSuccess(posts: $posts)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchingSuccessImpl &&
            (identical(other.posts, posts) || other.posts == posts));
  }

  @override
  int get hashCode => Object.hash(runtimeType, posts);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchingSuccessImplCopyWith<_$FetchingSuccessImpl> get copyWith =>
      __$$FetchingSuccessImplCopyWithImpl<_$FetchingSuccessImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingInProgress,
    required TResult Function(KtList<Post> posts) fetchingSuccess,
    required TResult Function(UserActionsFailure userActionsFailure)
        fetchingFailed,
  }) {
    return fetchingSuccess(posts);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fetchingInProgress,
    TResult? Function(KtList<Post> posts)? fetchingSuccess,
    TResult? Function(UserActionsFailure userActionsFailure)? fetchingFailed,
  }) {
    return fetchingSuccess?.call(posts);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingInProgress,
    TResult Function(KtList<Post> posts)? fetchingSuccess,
    TResult Function(UserActionsFailure userActionsFailure)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingSuccess != null) {
      return fetchingSuccess(posts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FetchingInProgress value) fetchingInProgress,
    required TResult Function(_FetchingSuccess value) fetchingSuccess,
    required TResult Function(_FetchingFailed value) fetchingFailed,
  }) {
    return fetchingSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_FetchingInProgress value)? fetchingInProgress,
    TResult? Function(_FetchingSuccess value)? fetchingSuccess,
    TResult? Function(_FetchingFailed value)? fetchingFailed,
  }) {
    return fetchingSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FetchingInProgress value)? fetchingInProgress,
    TResult Function(_FetchingSuccess value)? fetchingSuccess,
    TResult Function(_FetchingFailed value)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingSuccess != null) {
      return fetchingSuccess(this);
    }
    return orElse();
  }
}

abstract class _FetchingSuccess implements PostsFetcherState {
  const factory _FetchingSuccess({required final KtList<Post> posts}) =
      _$FetchingSuccessImpl;

  KtList<Post> get posts;
  @JsonKey(ignore: true)
  _$$FetchingSuccessImplCopyWith<_$FetchingSuccessImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FetchingFailedImplCopyWith<$Res> {
  factory _$$FetchingFailedImplCopyWith(_$FetchingFailedImpl value,
          $Res Function(_$FetchingFailedImpl) then) =
      __$$FetchingFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({UserActionsFailure userActionsFailure});

  $UserActionsFailureCopyWith<$Res> get userActionsFailure;
}

/// @nodoc
class __$$FetchingFailedImplCopyWithImpl<$Res>
    extends _$PostsFetcherStateCopyWithImpl<$Res, _$FetchingFailedImpl>
    implements _$$FetchingFailedImplCopyWith<$Res> {
  __$$FetchingFailedImplCopyWithImpl(
      _$FetchingFailedImpl _value, $Res Function(_$FetchingFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userActionsFailure = null,
  }) {
    return _then(_$FetchingFailedImpl(
      userActionsFailure: null == userActionsFailure
          ? _value.userActionsFailure
          : userActionsFailure // ignore: cast_nullable_to_non_nullable
              as UserActionsFailure,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $UserActionsFailureCopyWith<$Res> get userActionsFailure {
    return $UserActionsFailureCopyWith<$Res>(_value.userActionsFailure,
        (value) {
      return _then(_value.copyWith(userActionsFailure: value));
    });
  }
}

/// @nodoc

class _$FetchingFailedImpl implements _FetchingFailed {
  const _$FetchingFailedImpl({required this.userActionsFailure});

  @override
  final UserActionsFailure userActionsFailure;

  @override
  String toString() {
    return 'PostsFetcherState.fetchingFailed(userActionsFailure: $userActionsFailure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchingFailedImpl &&
            (identical(other.userActionsFailure, userActionsFailure) ||
                other.userActionsFailure == userActionsFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userActionsFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchingFailedImplCopyWith<_$FetchingFailedImpl> get copyWith =>
      __$$FetchingFailedImplCopyWithImpl<_$FetchingFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() fetchingInProgress,
    required TResult Function(KtList<Post> posts) fetchingSuccess,
    required TResult Function(UserActionsFailure userActionsFailure)
        fetchingFailed,
  }) {
    return fetchingFailed(userActionsFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? fetchingInProgress,
    TResult? Function(KtList<Post> posts)? fetchingSuccess,
    TResult? Function(UserActionsFailure userActionsFailure)? fetchingFailed,
  }) {
    return fetchingFailed?.call(userActionsFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? fetchingInProgress,
    TResult Function(KtList<Post> posts)? fetchingSuccess,
    TResult Function(UserActionsFailure userActionsFailure)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingFailed != null) {
      return fetchingFailed(userActionsFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FetchingInProgress value) fetchingInProgress,
    required TResult Function(_FetchingSuccess value) fetchingSuccess,
    required TResult Function(_FetchingFailed value) fetchingFailed,
  }) {
    return fetchingFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_FetchingInProgress value)? fetchingInProgress,
    TResult? Function(_FetchingSuccess value)? fetchingSuccess,
    TResult? Function(_FetchingFailed value)? fetchingFailed,
  }) {
    return fetchingFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FetchingInProgress value)? fetchingInProgress,
    TResult Function(_FetchingSuccess value)? fetchingSuccess,
    TResult Function(_FetchingFailed value)? fetchingFailed,
    required TResult orElse(),
  }) {
    if (fetchingFailed != null) {
      return fetchingFailed(this);
    }
    return orElse();
  }
}

abstract class _FetchingFailed implements PostsFetcherState {
  const factory _FetchingFailed(
          {required final UserActionsFailure userActionsFailure}) =
      _$FetchingFailedImpl;

  UserActionsFailure get userActionsFailure;
  @JsonKey(ignore: true)
  _$$FetchingFailedImplCopyWith<_$FetchingFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
