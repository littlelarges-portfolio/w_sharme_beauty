import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:w_sharme_beauty/domain/core/typedefs/typedefs.dart';
import 'package:w_sharme_beauty/domain/post/i_post_repository.dart';
import 'package:w_sharme_beauty/domain/post/post.dart';
import 'package:w_sharme_beauty/domain/post/post_failures.dart';
import 'package:w_sharme_beauty/domain/post/post_value_objects.dart';

part 'post_form_event.dart';
part 'post_form_state.dart';
part 'post_form_bloc.freezed.dart';

@injectable
class PostFormBloc extends Bloc<PostFormEvent, PostFormState> {
  PostFormBloc(this._postRepository) : super(PostFormState.initial()) {
    on<PostFormEvent>((event, emit) async {
      await event.map(
        contentChanged: (e) {
          emit(
            state.copyWith(
              post: state.post.copyWith(content: Content(e.contentStr)),
              publishFailureOrSuccess: none(),
            ),
          );
        },
        published: (e) async {
          PostRemoteRequest? publishFailureOrSuccess;

          emit(
            state.copyWith(
              isPublishing: true,
              publishFailureOrSuccess: none(),
            ),
          );

          if (state.post.failureOption.isNone()) {
            publishFailureOrSuccess = await _postRepository.publish(state.post);
          }

          emit(
            state.copyWith(
              isPublishing: false,
              showErrorMessages: AutovalidateMode.always,
              publishFailureOrSuccess: optionOf(publishFailureOrSuccess),
            ),
          );
        },
      );
    });
  }

  final IPostRepository _postRepository;
}
