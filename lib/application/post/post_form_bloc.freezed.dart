// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'post_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PostFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String contentStr) contentChanged,
    required TResult Function() published,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String contentStr)? contentChanged,
    TResult? Function()? published,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String contentStr)? contentChanged,
    TResult Function()? published,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ContentChanged value) contentChanged,
    required TResult Function(_Published value) published,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ContentChanged value)? contentChanged,
    TResult? Function(_Published value)? published,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ContentChanged value)? contentChanged,
    TResult Function(_Published value)? published,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostFormEventCopyWith<$Res> {
  factory $PostFormEventCopyWith(
          PostFormEvent value, $Res Function(PostFormEvent) then) =
      _$PostFormEventCopyWithImpl<$Res, PostFormEvent>;
}

/// @nodoc
class _$PostFormEventCopyWithImpl<$Res, $Val extends PostFormEvent>
    implements $PostFormEventCopyWith<$Res> {
  _$PostFormEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ContentChangedImplCopyWith<$Res> {
  factory _$$ContentChangedImplCopyWith(_$ContentChangedImpl value,
          $Res Function(_$ContentChangedImpl) then) =
      __$$ContentChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String contentStr});
}

/// @nodoc
class __$$ContentChangedImplCopyWithImpl<$Res>
    extends _$PostFormEventCopyWithImpl<$Res, _$ContentChangedImpl>
    implements _$$ContentChangedImplCopyWith<$Res> {
  __$$ContentChangedImplCopyWithImpl(
      _$ContentChangedImpl _value, $Res Function(_$ContentChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? contentStr = null,
  }) {
    return _then(_$ContentChangedImpl(
      null == contentStr
          ? _value.contentStr
          : contentStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ContentChangedImpl implements _ContentChanged {
  const _$ContentChangedImpl(this.contentStr);

  @override
  final String contentStr;

  @override
  String toString() {
    return 'PostFormEvent.contentChanged(contentStr: $contentStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ContentChangedImpl &&
            (identical(other.contentStr, contentStr) ||
                other.contentStr == contentStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, contentStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ContentChangedImplCopyWith<_$ContentChangedImpl> get copyWith =>
      __$$ContentChangedImplCopyWithImpl<_$ContentChangedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String contentStr) contentChanged,
    required TResult Function() published,
  }) {
    return contentChanged(contentStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String contentStr)? contentChanged,
    TResult? Function()? published,
  }) {
    return contentChanged?.call(contentStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String contentStr)? contentChanged,
    TResult Function()? published,
    required TResult orElse(),
  }) {
    if (contentChanged != null) {
      return contentChanged(contentStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ContentChanged value) contentChanged,
    required TResult Function(_Published value) published,
  }) {
    return contentChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ContentChanged value)? contentChanged,
    TResult? Function(_Published value)? published,
  }) {
    return contentChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ContentChanged value)? contentChanged,
    TResult Function(_Published value)? published,
    required TResult orElse(),
  }) {
    if (contentChanged != null) {
      return contentChanged(this);
    }
    return orElse();
  }
}

abstract class _ContentChanged implements PostFormEvent {
  const factory _ContentChanged(final String contentStr) = _$ContentChangedImpl;

  String get contentStr;
  @JsonKey(ignore: true)
  _$$ContentChangedImplCopyWith<_$ContentChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PublishedImplCopyWith<$Res> {
  factory _$$PublishedImplCopyWith(
          _$PublishedImpl value, $Res Function(_$PublishedImpl) then) =
      __$$PublishedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PublishedImplCopyWithImpl<$Res>
    extends _$PostFormEventCopyWithImpl<$Res, _$PublishedImpl>
    implements _$$PublishedImplCopyWith<$Res> {
  __$$PublishedImplCopyWithImpl(
      _$PublishedImpl _value, $Res Function(_$PublishedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PublishedImpl implements _Published {
  const _$PublishedImpl();

  @override
  String toString() {
    return 'PostFormEvent.published()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PublishedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String contentStr) contentChanged,
    required TResult Function() published,
  }) {
    return published();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String contentStr)? contentChanged,
    TResult? Function()? published,
  }) {
    return published?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String contentStr)? contentChanged,
    TResult Function()? published,
    required TResult orElse(),
  }) {
    if (published != null) {
      return published();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ContentChanged value) contentChanged,
    required TResult Function(_Published value) published,
  }) {
    return published(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ContentChanged value)? contentChanged,
    TResult? Function(_Published value)? published,
  }) {
    return published?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ContentChanged value)? contentChanged,
    TResult Function(_Published value)? published,
    required TResult orElse(),
  }) {
    if (published != null) {
      return published(this);
    }
    return orElse();
  }
}

abstract class _Published implements PostFormEvent {
  const factory _Published() = _$PublishedImpl;
}

/// @nodoc
mixin _$PostFormState {
  Post get post => throw _privateConstructorUsedError;
  bool get isPublishing =>
      throw _privateConstructorUsedError; // to showing progress
  AutovalidateMode get showErrorMessages => throw _privateConstructorUsedError;
  Option<Either<PostFailure, Unit>> get publishFailureOrSuccess =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PostFormStateCopyWith<PostFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostFormStateCopyWith<$Res> {
  factory $PostFormStateCopyWith(
          PostFormState value, $Res Function(PostFormState) then) =
      _$PostFormStateCopyWithImpl<$Res, PostFormState>;
  @useResult
  $Res call(
      {Post post,
      bool isPublishing,
      AutovalidateMode showErrorMessages,
      Option<Either<PostFailure, Unit>> publishFailureOrSuccess});

  $PostCopyWith<$Res> get post;
}

/// @nodoc
class _$PostFormStateCopyWithImpl<$Res, $Val extends PostFormState>
    implements $PostFormStateCopyWith<$Res> {
  _$PostFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? post = null,
    Object? isPublishing = null,
    Object? showErrorMessages = null,
    Object? publishFailureOrSuccess = null,
  }) {
    return _then(_value.copyWith(
      post: null == post
          ? _value.post
          : post // ignore: cast_nullable_to_non_nullable
              as Post,
      isPublishing: null == isPublishing
          ? _value.isPublishing
          : isPublishing // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      publishFailureOrSuccess: null == publishFailureOrSuccess
          ? _value.publishFailureOrSuccess
          : publishFailureOrSuccess // ignore: cast_nullable_to_non_nullable
              as Option<Either<PostFailure, Unit>>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $PostCopyWith<$Res> get post {
    return $PostCopyWith<$Res>(_value.post, (value) {
      return _then(_value.copyWith(post: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$PostStateImplCopyWith<$Res>
    implements $PostFormStateCopyWith<$Res> {
  factory _$$PostStateImplCopyWith(
          _$PostStateImpl value, $Res Function(_$PostStateImpl) then) =
      __$$PostStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Post post,
      bool isPublishing,
      AutovalidateMode showErrorMessages,
      Option<Either<PostFailure, Unit>> publishFailureOrSuccess});

  @override
  $PostCopyWith<$Res> get post;
}

/// @nodoc
class __$$PostStateImplCopyWithImpl<$Res>
    extends _$PostFormStateCopyWithImpl<$Res, _$PostStateImpl>
    implements _$$PostStateImplCopyWith<$Res> {
  __$$PostStateImplCopyWithImpl(
      _$PostStateImpl _value, $Res Function(_$PostStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? post = null,
    Object? isPublishing = null,
    Object? showErrorMessages = null,
    Object? publishFailureOrSuccess = null,
  }) {
    return _then(_$PostStateImpl(
      post: null == post
          ? _value.post
          : post // ignore: cast_nullable_to_non_nullable
              as Post,
      isPublishing: null == isPublishing
          ? _value.isPublishing
          : isPublishing // ignore: cast_nullable_to_non_nullable
              as bool,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      publishFailureOrSuccess: null == publishFailureOrSuccess
          ? _value.publishFailureOrSuccess
          : publishFailureOrSuccess // ignore: cast_nullable_to_non_nullable
              as Option<Either<PostFailure, Unit>>,
    ));
  }
}

/// @nodoc

class _$PostStateImpl implements _PostState {
  const _$PostStateImpl(
      {required this.post,
      required this.isPublishing,
      required this.showErrorMessages,
      required this.publishFailureOrSuccess});

  @override
  final Post post;
  @override
  final bool isPublishing;
// to showing progress
  @override
  final AutovalidateMode showErrorMessages;
  @override
  final Option<Either<PostFailure, Unit>> publishFailureOrSuccess;

  @override
  String toString() {
    return 'PostFormState(post: $post, isPublishing: $isPublishing, showErrorMessages: $showErrorMessages, publishFailureOrSuccess: $publishFailureOrSuccess)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PostStateImpl &&
            (identical(other.post, post) || other.post == post) &&
            (identical(other.isPublishing, isPublishing) ||
                other.isPublishing == isPublishing) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                other.showErrorMessages == showErrorMessages) &&
            (identical(
                    other.publishFailureOrSuccess, publishFailureOrSuccess) ||
                other.publishFailureOrSuccess == publishFailureOrSuccess));
  }

  @override
  int get hashCode => Object.hash(runtimeType, post, isPublishing,
      showErrorMessages, publishFailureOrSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PostStateImplCopyWith<_$PostStateImpl> get copyWith =>
      __$$PostStateImplCopyWithImpl<_$PostStateImpl>(this, _$identity);
}

abstract class _PostState implements PostFormState {
  const factory _PostState(
      {required final Post post,
      required final bool isPublishing,
      required final AutovalidateMode showErrorMessages,
      required final Option<Either<PostFailure, Unit>>
          publishFailureOrSuccess}) = _$PostStateImpl;

  @override
  Post get post;
  @override
  bool get isPublishing;
  @override // to showing progress
  AutovalidateMode get showErrorMessages;
  @override
  Option<Either<PostFailure, Unit>> get publishFailureOrSuccess;
  @override
  @JsonKey(ignore: true)
  _$$PostStateImplCopyWith<_$PostStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
