part of 'post_form_bloc.dart';

@freezed
abstract class PostFormState with _$PostFormState {
  const factory PostFormState({
    required Post post,
    required bool isPublishing, // to showing progress
    required AutovalidateMode showErrorMessages,
    required Option<PostRemoteRequest> publishFailureOrSuccess,
  }) = _PostState;

  factory PostFormState.initial() => PostFormState(
        post: Post.empty(),
        isPublishing: false,
        showErrorMessages: AutovalidateMode.disabled,
        publishFailureOrSuccess: none(),
      );
}
